package ru.kozyrev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.kozyrev.tm.api.endpoint.*;

import java.util.ArrayList;
import java.util.List;

class AdminEndpointTest {
    @NotNull
    private Context context;

    @BeforeEach
    void setUp() throws Throwable {
        context = new Context();
        context.setUp();
    }

    @AfterEach
    void tearDown() throws Throwable {
        context.tearDown();
    }

    @Test
    void adminXmlSaveFasterXML() throws Throwable {
        context.adminEndpoint.adminXmlSaveFasterXML(context.sessionBASE64);

        Assertions.assertEquals(9, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(3, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(2, context.adminEndpoint.userFindAll(context.sessionBASE64).size());

        context.adminEndpoint.projectRemoveAll(context.sessionBASE64);
        context.adminEndpoint.userRemoveAll(context.sessionBASE64);
        Assertions.assertEquals(0, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(0, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(1, context.adminEndpoint.userFindAll(context.sessionBASE64).size());

        context.adminEndpoint.adminXmlLoadFasterXML(context.sessionBASE64);

        Assertions.assertEquals(9, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(3, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(2, context.adminEndpoint.userFindAll(context.sessionBASE64).size());
    }

    @Test
    void adminXmlSaveLoadJaxB() throws Throwable {
        context.adminEndpoint.adminXmlSaveJaxB(context.sessionBASE64);

        Assertions.assertEquals(9, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(3, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(2, context.adminEndpoint.userFindAll(context.sessionBASE64).size());

        context.adminEndpoint.projectRemoveAll(context.sessionBASE64);
        context.adminEndpoint.userRemoveAll(context.sessionBASE64);
        Assertions.assertEquals(0, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(0, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(1, context.adminEndpoint.userFindAll(context.sessionBASE64).size());

        context.adminEndpoint.adminXmlLoadJaxB(context.sessionBASE64);

        Assertions.assertEquals(9, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(3, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(2, context.adminEndpoint.userFindAll(context.sessionBASE64).size());
    }

    @Test
    void adminBinSaveLoad() throws Throwable {
        context.adminEndpoint.adminBinSave(context.sessionBASE64);

        Assertions.assertEquals(9, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(3, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(2, context.adminEndpoint.userFindAll(context.sessionBASE64).size());

        context.adminEndpoint.projectRemoveAll(context.sessionBASE64);
        context.adminEndpoint.userRemoveAll(context.sessionBASE64);
        Assertions.assertEquals(0, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(0, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(1, context.adminEndpoint.userFindAll(context.sessionBASE64).size());

        context.adminEndpoint.adminBinLoad(context.sessionBASE64);

        Assertions.assertEquals(9, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(3, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(2, context.adminEndpoint.userFindAll(context.sessionBASE64).size());
    }

    @Test
    void userRemoveAll() throws Throwable {
        Assertions.assertEquals(2, context.adminEndpoint.userFindAll(context.sessionBASE64).size());
        context.adminEndpoint.userRemoveAll(context.sessionBASE64);
        Assertions.assertEquals(1, context.adminEndpoint.userFindAll(context.sessionBASE64).size());
    }

    @Test
    void adminJsonSaveLoadFasterXML() throws Throwable {
        context.adminEndpoint.adminJsonSaveFasterXML(context.sessionBASE64);
        Assertions.assertEquals(9, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(3, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(2, context.adminEndpoint.userFindAll(context.sessionBASE64).size());

        context.adminEndpoint.projectRemoveAll(context.sessionBASE64);
        context.adminEndpoint.userRemoveAll(context.sessionBASE64);
        Assertions.assertEquals(0, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(0, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(1, context.adminEndpoint.userFindAll(context.sessionBASE64).size());

        context.adminEndpoint.adminJsonLoadFasterXML(context.sessionBASE64);

        Assertions.assertEquals(9, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(3, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        Assertions.assertEquals(2, context.adminEndpoint.userFindAll(context.sessionBASE64).size());
    }

    @Test
    void adminJsonSaveLoadJaxB() throws Throwable {
        context.adminEndpoint.adminJsonSaveJaxB(context.sessionBASE64);

        context.adminEndpoint.adminJsonLoadJaxB(context.sessionBASE64);
    }

    @Test
    void taskRemoveAll() throws Throwable {
        Assertions.assertEquals(9, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        context.adminEndpoint.taskRemoveAll(context.sessionBASE64);
        Assertions.assertEquals(0, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
    }

    @Test
    void taskFindAll() throws Throwable {
        Assertions.assertEquals(9, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
    }

    @Test
    void projectRemoveAll() throws Throwable {
        Assertions.assertEquals(3, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        context.adminEndpoint.projectRemoveAll(context.sessionBASE64);
        Assertions.assertEquals(0, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
    }

    @Test
    void projectFindAll() throws Throwable {
        Assertions.assertEquals(3, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
    }

    @Test
    void userPersistList() throws Throwable {
        Assertions.assertEquals(2, context.adminEndpoint.userFindAll(context.sessionBASE64).size());
        @NotNull final List<UserDTO> list = new ArrayList<>();
        list.add(context.user1);
        list.add(context.user2);
        list.add(context.user3);
        context.adminEndpoint.userPersistList(context.sessionBASE64, list);
        Assertions.assertEquals(5, context.adminEndpoint.userFindAll(context.sessionBASE64).size());
    }

    @Test
    void taskPersistList() throws Throwable {
        projectPersistList();
        @NotNull final List<TaskDTO> list = new ArrayList<>();
        list.add(context.task1);
        list.add(context.task2);
        list.add(context.task3);
        context.adminEndpoint.taskPersistList(context.sessionBASE64, list);
        Assertions.assertEquals(12, context.adminEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
    }

    @Test
    void projectPersistList() throws Throwable {
        userPersistList();
        @NotNull final List<ProjectDTO> list = new ArrayList<>();
        list.add(context.project1);
        list.add(context.project2);
        list.add(context.project3);
        context.adminEndpoint.projectPersistList(context.sessionBASE64, list);
        Assertions.assertEquals(6, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
    }

    @Test
    void userFindAll() throws Throwable {
        Assertions.assertEquals(2, context.adminEndpoint.userFindAll(context.sessionBASE64).size());
    }
}
