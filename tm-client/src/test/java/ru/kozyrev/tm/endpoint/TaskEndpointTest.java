package ru.kozyrev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.kozyrev.tm.api.endpoint.Column;
import ru.kozyrev.tm.api.endpoint.Direction;
import ru.kozyrev.tm.api.endpoint.TaskDTO;
import ru.kozyrev.tm.util.DateUtil;

import java.util.List;

class TaskEndpointTest {
    @NotNull
    private Context context;

    @BeforeEach
    void setUp() throws Throwable {
        context = new Context();
        context.setUp();
    }

    @AfterEach
    void tearDown() throws Throwable {
        context.tearDown();
    }

    @Test
    void taskFindAllByProjectAuthSortNumAsc() throws Throwable {
        @NotNull final List<TaskDTO> list =
                context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(3, list.size());

        Assertions.assertEquals("1", list.get(0).getName());
        Assertions.assertEquals("2", list.get(1).getName());
        Assertions.assertEquals("3", list.get(2).getName());
    }

    @Test
    void taskFindAllByProjectAuthSortNumDesc() throws Throwable {
        @NotNull final List<TaskDTO> list =
                context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.DESC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(3, list.size());

        Assertions.assertEquals("1", list.get(0).getName());
        Assertions.assertEquals("2", list.get(1).getName());
        Assertions.assertEquals("3", list.get(2).getName());
    }

    @Test
    void taskFindAllByProjectAuthSortStartAsc() throws Throwable {
        @NotNull final List<TaskDTO> list =
                context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.DATE_START, Direction.ASC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(3, list.size());

        Assertions.assertEquals("1", list.get(0).getName());
        Assertions.assertEquals("2", list.get(1).getName());
        Assertions.assertEquals("3", list.get(2).getName());
    }

    @Test
    void taskFindAllByProjectAuthSortStartDesc() throws Throwable {
        @NotNull final List<TaskDTO> list =
                context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.DATE_START, Direction.DESC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(3, list.size());

        Assertions.assertEquals("1", list.get(0).getName());
        Assertions.assertEquals("2", list.get(1).getName());
        Assertions.assertEquals("3", list.get(2).getName());
    }

    @Test
    void taskFindAllByProjectAuthSortFinishAsc() throws Throwable {
        @NotNull final List<TaskDTO> list =
                context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.DATE_FINISH, Direction.ASC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(3, list.size());

        Assertions.assertEquals("1", list.get(0).getName());
        Assertions.assertEquals("2", list.get(1).getName());
        Assertions.assertEquals("3", list.get(2).getName());
    }

    @Test
    void taskFindAllByProjectAuthSortFinishDesc() throws Throwable {
        @NotNull final List<TaskDTO> list =
                context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.DATE_FINISH, Direction.DESC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(3, list.size());

        Assertions.assertEquals("1", list.get(0).getName());
        Assertions.assertEquals("2", list.get(1).getName());
        Assertions.assertEquals("3", list.get(2).getName());
    }

    @Test
    void taskFindAllByProjectAuthSortStatusAsc() throws Throwable {
        @NotNull final List<TaskDTO> list =
                context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.STATUS, Direction.ASC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(3, list.size());

        Assertions.assertEquals("1", list.get(0).getName());
        Assertions.assertEquals("2", list.get(1).getName());
        Assertions.assertEquals("3", list.get(2).getName());
    }

    @Test
    void taskFindAllByProjectAuthSortStatusDesc() throws Throwable {
        @NotNull final List<TaskDTO> list =
                context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.STATUS, Direction.DESC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(3, list.size());

        Assertions.assertEquals("1", list.get(0).getName());
        Assertions.assertEquals("2", list.get(1).getName());
        Assertions.assertEquals("3", list.get(2).getName());
    }

    @Test
    void taskPersist() throws Throwable {
        @NotNull final List<TaskDTO> list =
                context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.DATE_START, Direction.ASC);
        Assertions.assertEquals(5, list.size());
        context.userEndpoint.userPersist(context.user1);
        context.projectEndpoint.projectPersist(context.sessionBASE64, context.project1);
        context.taskEndpoint.taskPersist(context.sessionBASE64, context.task1);
        @NotNull final List<TaskDTO> list2 =
                context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.DATE_START, Direction.ASC);

        Assertions.assertEquals(5, list2.size());
    }

    @Test
    void taskRemoveAllByProject() throws Throwable {
        Assertions.assertEquals(3, context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        context.taskEndpoint.taskRemoveAllByProject(context.sessionBASE64, context.project1Num);
        Assertions.assertEquals(0, context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
    }

    @Test
    void taskFindAll() throws Throwable {
        Assertions.assertEquals(8, context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
    }

    @Test
    void taskRemove() throws Throwable {
        Assertions.assertEquals(8, context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        context.taskEndpoint.taskRemove(context.sessionBASE64, context.task7Id);
        Assertions.assertEquals(7, context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
    }

    @Test
    void taskMerge() throws Throwable {
        @Nullable final TaskDTO testTask = context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).get(4);
        Assertions.assertNotNull(testTask);
        testTask.setName("Новое имя");
        testTask.setDescription("Новое описание");
        testTask.setDateStart(DateUtil.parseDateXML("12-12-2013"));
        testTask.setDateFinish(DateUtil.parseDateXML("01-01-2013"));
        context.taskEndpoint.taskMerge(context.sessionBASE64, testTask);
        @Nullable final TaskDTO taskDB = context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).get(4);
        Assertions.assertEquals("Новое имя", taskDB.getName());
        Assertions.assertEquals("Новое описание", taskDB.getDescription());
        Assertions.assertEquals(DateUtil.parseDateXML("12-12-2013"), taskDB.getDateStart());
        Assertions.assertEquals(DateUtil.parseDateXML("01-01-2013"), taskDB.getDateFinish());
    }

    @Test
    void taskRemoveAll() throws Throwable {
        context.taskEndpoint.taskRemoveAll(context.sessionBASE64);
        Assertions.assertEquals(0, context.taskEndpoint.taskFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
    }

    @Test
    void taskFindWord() throws Throwable {
        Assertions.assertEquals(3, context.taskEndpoint.taskFindWord(context.sessionBASE64, "дан").size());
    }
}