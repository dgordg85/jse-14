package ru.kozyrev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.kozyrev.tm.api.endpoint.Column;
import ru.kozyrev.tm.api.endpoint.Direction;
import ru.kozyrev.tm.api.endpoint.DocumentStatus;
import ru.kozyrev.tm.api.endpoint.ProjectDTO;
import ru.kozyrev.tm.util.DateUtil;

import java.util.List;

class ProjectEndpointTest {
    @NotNull
    private Context context;

    @BeforeEach
    void setUp() throws Throwable {
        context = new Context();
        context.setUp();
    }

    @AfterEach
    void tearDown() throws Throwable {
        context.tearDown();
    }

    @Test
    void projectRemove() throws Throwable {
        Assertions.assertEquals(2, context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        context.projectEndpoint.projectRemove(context.sessionBASE64, context.listProjects.get(0).getId());
        Assertions.assertEquals(1, context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
    }

    @Test
    void projectPersist() throws Throwable {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("Новое имя проекта");
        projectDTO.setUserId(context.adminId);
        projectDTO.setDescription("Описание проекта изменено");
        projectDTO.setDateStart(DateUtil.parseDateXML("01-01-1999"));
        projectDTO.setDateFinish(DateUtil.parseDateXML("01-01-2001"));
        projectDTO.setStatus(DocumentStatus.IN_PROGRESS);
        context.projectEndpoint.projectPersist(context.sessionBASE64, projectDTO);

        @NotNull final ProjectDTO projectDB = context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).get(3);
        Assertions.assertEquals("Новое имя проекта", projectDB.getName());
        Assertions.assertEquals("Описание проекта изменено", projectDB.getDescription());
        Assertions.assertEquals(DateUtil.parseDateXML("01-01-1999"), projectDB.getDateStart());
        Assertions.assertEquals(DateUtil.parseDateXML("01-01-2001"), projectDB.getDateFinish());
        Assertions.assertEquals(DocumentStatus.IN_PROGRESS, projectDB.getStatus());

    }

    @Test
    void projectRemoveAll() throws Throwable {
        context.projectEndpoint.projectRemoveAll(context.sessionBASE64);
        Assertions.assertEquals(0, context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
    }

    @Test
    void projectFindWord() throws Throwable {
        Assertions.assertEquals(2, context.projectEndpoint.projectFindWord(context.sessionBASE64, "проект").size());
    }

    @Test
    void projectFindAll() throws Throwable {
        Assertions.assertEquals(2, context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
    }

    @Test
    void projectMerge() throws Throwable {
        @Nullable final ProjectDTO projectDTO = context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).get(1);
        projectDTO.setName("Новое имя проекта");
        projectDTO.setDescription("Описание проекта изменено");
        projectDTO.setDateStart(DateUtil.parseDateXML("01-01-1999"));
        projectDTO.setDateFinish(DateUtil.parseDateXML("01-01-2001"));
        projectDTO.setStatus(DocumentStatus.IN_PROGRESS);
        context.projectEndpoint.projectMerge(context.sessionBASE64, projectDTO);

        @Nullable final ProjectDTO projectDB = context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).get(1);
        Assertions.assertEquals("Новое имя проекта", projectDB.getName());
        Assertions.assertEquals("Описание проекта изменено", projectDB.getDescription());
        Assertions.assertEquals(DateUtil.parseDateXML("01-01-1999"), projectDB.getDateStart());
        Assertions.assertEquals(DateUtil.parseDateXML("01-01-2001"), projectDB.getDateFinish());
        Assertions.assertEquals(DocumentStatus.IN_PROGRESS, projectDB.getStatus());
    }

    @Test
    void projectFindAllBySortingNumAsc() throws Throwable {
        @NotNull final List<ProjectDTO> list =
                context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(2, list.size());

        Assertions.assertEquals("1", list.get(0).getName());
        Assertions.assertEquals("2", list.get(1).getName());
    }

    @Test
    void projectFindAllBySortingNumDesc() throws Throwable {
        @NotNull final List<ProjectDTO> list =
                context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.DESC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(2, list.size());

        Assertions.assertEquals("2", list.get(0).getName());
        Assertions.assertEquals("1", list.get(1).getName());
    }

    @Test
    void projectFindAllBySortingStartAsc() throws Throwable {
        @NotNull final List<ProjectDTO> list =
                context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.DATE_START, Direction.ASC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(2, list.size());

        Assertions.assertEquals("2", list.get(0).getDateStart().toString());
        Assertions.assertEquals("1", list.get(1).getDateStart().toString());
    }

    @Test
    void projectFindAllBySortingStartDesc() throws Throwable {
        @NotNull final List<ProjectDTO> list =
                context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.DATE_START, Direction.DESC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(2, list.size());

        Assertions.assertEquals("2", list.get(0).getDateStart().toString());
        Assertions.assertEquals("1", list.get(1).getDateStart().toString());
    }

    @Test
    void projectFindAllBySortingFinishAsc() throws Throwable {
        @NotNull final List<ProjectDTO> list =
                context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.DATE_FINISH, Direction.ASC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(2, list.size());

        Assertions.assertEquals("2", list.get(0).getDateFinish().toString());
        Assertions.assertEquals("1", list.get(1).getDateFinish().toString());
    }

    @Test
    void projectFindAllBySortingFinishDesc() throws Throwable {
        @NotNull final List<ProjectDTO> list =
                context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.DATE_FINISH, Direction.DESC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(2, list.size());

        Assertions.assertEquals("2", list.get(0).getDateFinish());
        Assertions.assertEquals("1", list.get(1).getDateFinish());
    }

    @Test
    void projectFindAllBySortingStatusAsc() throws Throwable {
        @NotNull final List<ProjectDTO> list =
                context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.STATUS, Direction.ASC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(2, list.size());

        Assertions.assertEquals("2", list.get(0).getStatus().toString());
        Assertions.assertEquals("1", list.get(1).getStatus().toString());
    }

    @Test
    void projectFindAllBySortingStatusDesc() throws Throwable {
        @NotNull final List<ProjectDTO> list =
                context.projectEndpoint.projectFindAll(context.sessionBASE64, Column.STATUS, Direction.DESC);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(2, list.size());

        Assertions.assertEquals("1", list.get(0).getStatus().toString());
        Assertions.assertEquals("2", list.get(1).getStatus().toString());
    }
}
