package ru.kozyrev.tm.endpoint;


import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.kozyrev.tm.api.endpoint.Column;
import ru.kozyrev.tm.api.endpoint.Direction;
import ru.kozyrev.tm.api.endpoint.UserDTO;
import ru.kozyrev.tm.util.HashUtil;

import javax.xml.ws.WebServiceException;

import static org.junit.jupiter.api.Assertions.assertThrows;

class SessionEndpointTest {
    @NotNull
    private Context context;

    @BeforeEach
    void setUp() throws Throwable {
        context = new Context();
        context.setUp();
    }

    @AfterEach
    void tearDown() throws Throwable {
        context.tearDown();
    }

    @Test
    void openSession() throws Throwable {
        @NotNull final UserDTO user = new UserDTO();
        @NotNull final String password = HashUtil.getHash("TEST");
        user.setLogin("TEST");
        user.setPasswordHash(password);

        context.userEndpoint.userPersist(user);
        context.sessionEndpoint.openSession("TEST", password);
    }

    @Test
    void closeSession() throws Throwable {
        Assertions.assertEquals(3, context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC).size());
        context.sessionEndpoint.closeSession(context.getSessionBASE64());

        @NotNull final WebServiceException exception = assertThrows(WebServiceException.class,
                () -> context.adminEndpoint.projectFindAll(context.sessionBASE64, Column.NUM, Direction.ASC)
        );
        Assertions.assertTrue(exception.getMessage().contains("Session not valid!"));
    }

}