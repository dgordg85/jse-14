package ru.kozyrev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.kozyrev.tm.api.endpoint.UserDTO;
import ru.kozyrev.tm.util.HashUtil;

import java.util.List;


class UserEndpointTest {
    @NotNull
    private Context context;

    @BeforeEach
    void setUp() throws Throwable {
        context = new Context();
        context.setUp();
    }

    @AfterEach
    void tearDown() throws Throwable {
        context.tearDown();
    }

    @Test
    void userPersist() throws Throwable {
        context.userEndpoint.userPersist(context.user1);
        @Nullable final List<UserDTO> list = context.adminEndpoint.userFindAll(context.sessionBASE64);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(3, list.size());
        Assertions.assertNotNull(context.userEndpoint.userFindOne(context.sessionBASE64, context.user1.getId()));

    }

    @Test
    void userUpdatePassword() throws Throwable {
        @NotNull final String oldPass = "TEST";
        @NotNull final String newPass = "BRHR";
        context.createTestUserAddDbCreateSession(oldPass);
        @Nullable final UserDTO admin = new UserDTO();
        admin.setId(context.bootstrap.getStateService().getUserId());
        admin.setPasswordHash(HashUtil.getHash(newPass));
        context.userEndpoint.userUpdatePassword(context.sessionBASE64, admin, oldPass);
        Assertions.assertNotNull(context.sessionEndpoint.openSession(oldPass, newPass));
    }

    @Test
    void userRemove() throws Throwable {
        context.userEndpoint.userPersist(context.user3);
        Assertions.assertEquals(3, context.adminEndpoint.userFindAll(context.sessionBASE64).size());
        context.userEndpoint.userRemove(context.sessionBASE64, context.user3.getId());
        Assertions.assertEquals(2, context.adminEndpoint.userFindAll(context.sessionBASE64).size());
    }

    @Test
    void userFindOne() throws Throwable {
        context.persistUserCreateSession(context.user3);
        Assertions.assertNotNull(context.userEndpoint.userFindOne(context.sessionBASE64, context.user3.getId()));
    }

//    @Test
//    void userMergeLogin() throws Throwable {
//        context.persistUserCreateSession(context.user1);
//        @NotNull final User user = context.userEndpoint.userGetYourself(context.session);
//        user.setLogin("User55");
//        user.setPasswordHash("");
//        context.userEndpoint.userMerge(context.session, user);
//        Assertions.assertNotNull(context.sessionEndpoint.openSession("User55", context.user1.getPasswordHash()));
//    }
//
//    @Test
//    void userMergePass() throws Throwable {
//        context.persistUserCreateSession(context.user1);
//        @NotNull final User user = context.userEndpoint.userGetYourself(context.session);
//        user.setPasswordHash(HashUtil.getHash("User55"));
//        context.userEndpoint.userMerge(context.session, user);
//        Assertions.assertNotNull(context.sessionEndpoint.openSession(
//                context.user1.getLogin(), HashUtil.getHash("User55")));
//    }
}
