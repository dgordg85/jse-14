package ru.kozyrev.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.*;
import ru.kozyrev.tm.context.Bootstrap;
import ru.kozyrev.tm.util.DateUtil;
import ru.kozyrev.tm.util.HashUtil;

import java.util.List;

@Getter
@Setter
public class Context {
    @NotNull
    public final Bootstrap bootstrap;
    @NotNull
    public final IAdminEndpoint adminEndpoint;
    @NotNull
    public final IProjectEndpoint projectEndpoint;
    @NotNull
    public final ISessionEndpoint sessionEndpoint;
    @NotNull
    public final ITaskEndpoint taskEndpoint;
    @NotNull
    public final IUserEndpoint userEndpoint;
    @Nullable
    public String sessionBASE64 = null;
    @NotNull ProjectDTO project1;
    @NotNull ProjectDTO project2;
    @NotNull ProjectDTO project3;
    @NotNull TaskDTO task1;
    @NotNull TaskDTO task2;
    @NotNull TaskDTO task3;
    @NotNull String project1Num;
    @NotNull String project2Num;
    @NotNull String project3Id;
    @NotNull String task1Id;
    @NotNull String task2Id;
    @NotNull String task3Id;
    @NotNull String task4Id;
    @NotNull String task5Id;
    @NotNull String task6Id;
    @NotNull String task7Id;
    @NotNull String task8Id;
    @NotNull String task9Id;
    @NotNull String task10Id;
    @NotNull String task11Id;
    @NotNull String task12Id;
    @NotNull UserDTO user1;
    @NotNull UserDTO user2;
    @NotNull UserDTO user3;

    @NotNull String adminId;
    @NotNull String userId;

    @Nullable List<ProjectDTO> listTasks = null;
    @Nullable List<TaskDTO> listProjects = null;


    public Context() {
        bootstrap = new Bootstrap();
        adminEndpoint = bootstrap.getAdminEndpoint();
        projectEndpoint = bootstrap.getProjectEndpoint();
        sessionEndpoint = bootstrap.getSessionEndpoint();
        taskEndpoint = bootstrap.getTaskEndpoint();
        userEndpoint = bootstrap.getUserEndpoint();
    }

    void setUp() throws Throwable {
        tearDown();

        createUsers();
        createProjects();
        createTasks();

        createSessionAdmin();
        createUser();
        fillList();
        listTasks = projectEndpoint.projectFindAll(sessionBASE64, Column.NUM, Direction.ASC);
        listProjects = taskEndpoint.taskFindAll(sessionBASE64, Column.NUM, Direction.ASC);
    }

    void tearDown() throws Throwable {
        createSessionAdmin();
        adminEndpoint.projectRemoveAll(sessionBASE64);
        adminEndpoint.taskRemoveAll(sessionBASE64);
        adminEndpoint.userRemoveAll(sessionBASE64);
        sessionEndpoint.closeSession(sessionBASE64);
        sessionBASE64 = null;
    }

    public final void createUser() throws Throwable {
        @Nullable final UserDTO user = new UserDTO();
        user.setLogin("user");
        user.setPasswordHash(HashUtil.getHash("user"));
        user.setRoleType(RoleType.USER);
        userEndpoint.userPersist(user);
        userId = userEndpoint.userFindOneByLogin(sessionBASE64, "user").getId();
    }

    public void createSessionAdmin() throws Throwable {
        @NotNull final String passwordHash = HashUtil.getHash("admin");
        sessionBASE64 = sessionEndpoint.openSession("admin", passwordHash);
        adminId = userEndpoint.userFindOneByLogin(sessionBASE64, "admin").getId();
    }

    public void createSessionUser() throws Throwable {
        @NotNull final String passwordHash = HashUtil.getHash("user");
        sessionBASE64 = sessionEndpoint.openSession("user", passwordHash);
        userId = userEndpoint.userFindOneByLogin(sessionBASE64, "user").getId();
    }

    public void createTestUserAddDbCreateSession(@NotNull final String loginPass) throws Throwable {
        @NotNull final String login = loginPass;
        @NotNull final String passwordHash = HashUtil.getHash(loginPass);
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        userEndpoint.userPersist(user);
        sessionBASE64 = sessionEndpoint.openSession(login, passwordHash);
    }

    public void persistUserCreateSession(@NotNull final UserDTO user) throws Throwable {
        @NotNull final String login = user.getLogin();
        @NotNull final String passwordHash = user.getPasswordHash();
        userEndpoint.userPersist(user);
        sessionBASE64 = sessionEndpoint.openSession(login, passwordHash);
    }

    public void fillList() throws Throwable {
        createSessionAdmin();
        @NotNull final ProjectDTO project1 = new ProjectDTO();
        project1.setName("Проект 1");
        project1.setUserId(adminId);
        project1.setDescription("Удаление данных");
        project1.setDateStart(DateUtil.parseDateXML("30-04-2020"));
        project1.setDateFinish(DateUtil.parseDateXML("09-04-2020"));
        project1.setStatus(DocumentStatus.READY);

        @NotNull final ProjectDTO project2 = new ProjectDTO();
        project2.setName("Проект 2");
        project2.setUserId(adminId);
        project2.setDescription("Восстановление");
        project2.setDateStart(DateUtil.parseDateXML("10-04-2020"));
        project2.setDateFinish(DateUtil.parseDateXML("05-05-2020"));
        project1.setStatus(DocumentStatus.IN_PROGRESS);

        projectEndpoint.projectPersist(sessionBASE64, project1);
        projectEndpoint.projectPersist(sessionBASE64, project2);

        @NotNull final TaskDTO task1 = new TaskDTO();
        task1.setName("Задача 1");
        task1.setDescription("Получить доступы");
        task1.setUserId(adminId);
        task1.setProjectId(listProjects.get(0).getId());
        task1.setDateStart(DateUtil.parseDateXML("01-04-2020"));
        task1.setDateFinish(DateUtil.parseDateXML("25-04-2020"));
        task1.setStatus(DocumentStatus.READY);
        taskEndpoint.taskPersist(sessionBASE64, task1);


        @NotNull final TaskDTO task2 = new TaskDTO();
        task2.setName("Задача 2");
        task2.setUserId(adminId);
        task2.setProjectId(listProjects.get(0).getId());
        task2.setDescription("Удалить данные");
        task2.setDateStart(DateUtil.parseDateXML("05-04-2020"));
        task2.setDateFinish(DateUtil.parseDateXML("06-04-2020"));
        task2.setStatus(DocumentStatus.PLANNED);
        taskEndpoint.taskPersist(sessionBASE64, task2);


        @NotNull final TaskDTO task3 = new TaskDTO();
        task3.setName("Задача 3");
        task3.setUserId(adminId);
        task3.setProjectId(listProjects.get(0).getId());
        task3.setDescription("Восстановить данные");
        task3.setDateStart(DateUtil.parseDateXML("07-04-2020"));
        task3.setDateFinish(DateUtil.parseDateXML("09-04-2020"));
        task3.setStatus(DocumentStatus.IN_PROGRESS);
        taskEndpoint.taskPersist(sessionBASE64, task3);


        @NotNull final TaskDTO task4 = new TaskDTO();
        task4.setName("Задача 1");
        task4.setUserId(adminId);
        task4.setProjectId(listProjects.get(1).getId());
        task4.setDescription("Получить вводные");
        task4.setDateStart(DateUtil.parseDateXML("10-04-2020"));
        task4.setDateFinish(DateUtil.parseDateXML("11-04-2020"));
        task4.setStatus(DocumentStatus.READY);
        taskEndpoint.taskPersist(sessionBASE64, task4);


        @NotNull final TaskDTO task5 = new TaskDTO();
        task5.setName("Задача 2");
        task5.setUserId(adminId);
        task5.setProjectId(listProjects.get(1).getId());
        task5.setDescription("Сделать бекап");
        task5.setDateStart(DateUtil.parseDateXML("12-04-2020"));
        task5.setDateFinish(DateUtil.parseDateXML("12-04-2020"));
        task5.setStatus(DocumentStatus.READY);
        taskEndpoint.taskPersist(sessionBASE64, task5);


        @NotNull final TaskDTO task6 = new TaskDTO();
        task6.setName("Задача 3");
        task6.setUserId(adminId);
        task6.setProjectId(listProjects.get(1).getId());
        task6.setDescription("Проверить данные");
        task6.setDateStart(DateUtil.parseDateXML("13-04-2020"));
        task6.setDateFinish(DateUtil.parseDateXML("15-04-2020"));
        task6.setStatus(DocumentStatus.PLANNED);
        taskEndpoint.taskPersist(sessionBASE64, task6);


        @NotNull final TaskDTO task7 = new TaskDTO();
        task7.setName("Задача 4");
        task7.setUserId(adminId);
        task7.setProjectId(listProjects.get(1).getId());
        task7.setDescription("Попытаться восстановить");
        task7.setDateStart(DateUtil.parseDateXML("16-04-2020"));
        task7.setDateFinish(DateUtil.parseDateXML("27-04-2020"));
        task7.setStatus(DocumentStatus.READY);
        taskEndpoint.taskPersist(sessionBASE64, task7);


        @NotNull final TaskDTO task8 = new TaskDTO();
        task8.setName("Задача 5");
        task8.setUserId(adminId);
        task8.setProjectId(listProjects.get(1).getId());
        task8.setDescription("Передать клиенту");
        task8.setDateStart(DateUtil.parseDateXML("28-04-2020"));
        task8.setDateFinish(DateUtil.parseDateXML("05-05-2020"));
        task8.setStatus(DocumentStatus.PLANNED);
        taskEndpoint.taskPersist(sessionBASE64, task8);


        createSessionUser();

        @NotNull final ProjectDTO project3 = new ProjectDTO();
        project3.setName("Проект 3");
        project3.setUserId(userId);
        project3.setDescription("Просто проект");
        project3.setDateStart(DateUtil.parseDateXML("22-02-2022"));
        project3.setDateFinish(DateUtil.parseDateXML("23-03-2023"));
        projectEndpoint.projectPersist(sessionBASE64, project3);


        @NotNull final TaskDTO task9 = new TaskDTO();
        task9.setName("Задача 1");
        task9.setUserId(userId);
        task9.setProjectId(listProjects.get(0).getId());
        task9.setDescription("Просто таск");
        task9.setDateStart(DateUtil.parseDateXML("01-04-2020"));
        task9.setDateFinish(DateUtil.parseDateXML("04-04-2020"));
        task9.setStatus(DocumentStatus.IN_PROGRESS);
        taskEndpoint.taskPersist(sessionBASE64, task9);

        createSessionAdmin();
    }

    void createUsers() {
        @NotNull final String loginPass1 = "user1";
        user1 = new UserDTO();
        user1.setLogin(loginPass1);
        user1.setPasswordHash(HashUtil.getHash(loginPass1));
        user1.setRoleType(RoleType.USER);
        user1.setId("98ebe1bf-eb74-4db5-a835-dd7f47a1e21b");

        @NotNull final String loginPass2 = "user2";
        user2 = new UserDTO();
        user2.setLogin(loginPass2);
        user2.setPasswordHash(HashUtil.getHash(loginPass2));
        user2.setRoleType(RoleType.USER);
        user2.setId("4ea9da55-52f1-47e7-9c5e-ee44a0a95819");

        @NotNull final String loginPass3 = "user3";
        user3 = new UserDTO();
        user3.setLogin(loginPass3);
        user3.setPasswordHash(HashUtil.getHash(loginPass3));
        user3.setRoleType(RoleType.ADMIN);
        user3.setId("7f4ae477-2293-4026-b95a-0ce9de07c016");
    }

    void createProjects() throws java.lang.Exception {
        project1 = new ProjectDTO();
        project1.setId("6bf55f5c-2016-4028-b78f-7b73dca82689");
        project1.setName("Тестовый проект 1");
        project1.setDescription("Тестовое описание 2");
        project1.setDateStart(DateUtil.parseDateXML("09-09-99"));
        project1.setDateFinish(DateUtil.parseDateXML("01-01-01"));
        project1.setStatus(DocumentStatus.IN_PROGRESS);
        project1.setUserId("98ebe1bf-eb74-4db5-a835-dd7f47a1e21b");

        project2 = new ProjectDTO();
        project2.setId("bf141e28-b6dd-45a8-b2ae-a49514e32f38");
        project2.setName("Тестовый проект 2");
        project2.setDescription("Тестовое описание 2");
        project2.setDateStart(DateUtil.parseDateXML("08-08-88"));
        project2.setDateFinish(DateUtil.parseDateXML("12-12-12"));
        project2.setStatus(DocumentStatus.READY);
        project2.setUserId("4ea9da55-52f1-47e7-9c5e-ee44a0a95819");

        project3 = new ProjectDTO();
        project3.setId("cf1328d7-26d0-4036-accf-cf596c9722ed");
        project3.setName("Тестовый проект 3");
        project3.setDescription("Тестовое описание 3");
        project3.setDateStart(DateUtil.parseDateXML("07-07-77"));
        project3.setDateFinish(DateUtil.parseDateXML("11-11-11"));
        project3.setStatus(DocumentStatus.PLANNED);
        project3.setUserId("7f4ae477-2293-4026-b95a-0ce9de07c016");
    }

    void createTasks() throws java.lang.Exception {
        task1 = new TaskDTO();
        task1.setName("Тестовый таск 1");
        task1.setDescription("Тестовое описание 1");
        task1.setProjectId("6bf55f5c-2016-4028-b78f-7b73dca82689");
        task1.setDateStart(DateUtil.parseDateXML("09-09-99"));
        task1.setDateFinish(DateUtil.parseDateXML("01-01-01"));
        task1.setStatus(DocumentStatus.IN_PROGRESS);
        task1.setUserId("98ebe1bf-eb74-4db5-a835-dd7f47a1e21b");

        task2 = new TaskDTO();
        task2.setName("Тестовый таск 2");
        task2.setDescription("Тестовое описание 2");
        task2.setProjectId("bf141e28-b6dd-45a8-b2ae-a49514e32f38");
        task2.setDateStart(DateUtil.parseDateXML("08-08-88"));
        task2.setDateFinish(DateUtil.parseDateXML("12-12-12"));
        task2.setStatus(DocumentStatus.READY);
        task2.setUserId("4ea9da55-52f1-47e7-9c5e-ee44a0a95819");

        task3 = new TaskDTO();
        task3.setProjectId("cf1328d7-26d0-4036-accf-cf596c9722ed");
        task3.setName("Тестовый таск 3");
        task3.setDescription("Тестовое описание 3");
        task3.setDateStart(DateUtil.parseDateXML("07-07-77"));
        task3.setDateFinish(DateUtil.parseDateXML("11-11-11"));
        task3.setStatus(DocumentStatus.PLANNED);
        task3.setUserId("7f4ae477-2293-4026-b95a-0ce9de07c016");
    }
}
