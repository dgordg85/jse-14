package ru.kozyrev.tm.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-06-15T00:49:22.562+03:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", name = "ITaskEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ITaskEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskRemoveAllByProjectRequest", output = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskRemoveAllByProjectResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskRemoveAllByProject/Fault/Exception")})
    @RequestWrapper(localName = "taskRemoveAllByProject", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskRemoveAllByProject")
    @ResponseWrapper(localName = "taskRemoveAllByProjectResponse", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskRemoveAllByProjectResponse")
    void taskRemoveAllByProject(
            @WebParam(name = "sessionBASE64", targetNamespace = "")
                    java.lang.String sessionBASE64,
            @WebParam(name = "projectId", targetNamespace = "")
                    java.lang.String projectId
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskPersistRequest", output = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskPersistResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskPersist/Fault/Exception")})
    @RequestWrapper(localName = "taskPersist", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskPersist")
    @ResponseWrapper(localName = "taskPersistResponse", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskPersistResponse")
    void taskPersist(
            @WebParam(name = "sessionBASE64", targetNamespace = "")
                    java.lang.String sessionBASE64,
            @WebParam(name = "arg1", targetNamespace = "")
                    ru.kozyrev.tm.api.endpoint.TaskDTO arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskMergeRequest", output = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskMergeResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskMerge/Fault/Exception")})
    @RequestWrapper(localName = "taskMerge", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskMerge")
    @ResponseWrapper(localName = "taskMergeResponse", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskMergeResponse")
    void taskMerge(
            @WebParam(name = "sessionBASE64", targetNamespace = "")
                    java.lang.String sessionBASE64,
            @WebParam(name = "arg1", targetNamespace = "")
                    ru.kozyrev.tm.api.endpoint.TaskDTO arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskRemoveAllRequest", output = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskRemoveAllResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskRemoveAll/Fault/Exception")})
    @RequestWrapper(localName = "taskRemoveAll", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskRemoveAll")
    @ResponseWrapper(localName = "taskRemoveAllResponse", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskRemoveAllResponse")
    void taskRemoveAll(
            @WebParam(name = "sessionBASE64", targetNamespace = "")
                    java.lang.String sessionBASE64
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskFindWordRequest", output = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskFindWordResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskFindWord/Fault/Exception")})
    @RequestWrapper(localName = "taskFindWord", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskFindWord")
    @ResponseWrapper(localName = "taskFindWordResponse", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskFindWordResponse")
    @WebResult(name = "return", targetNamespace = "")
    java.util.List<ru.kozyrev.tm.api.endpoint.TaskDTO> taskFindWord(
            @WebParam(name = "sessionBASE64", targetNamespace = "")
                    java.lang.String sessionBASE64,
            @WebParam(name = "word", targetNamespace = "")
                    java.lang.String word
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskFindAllRequest", output = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskFindAllResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskFindAll/Fault/Exception")})
    @RequestWrapper(localName = "taskFindAll", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskFindAll")
    @ResponseWrapper(localName = "taskFindAllResponse", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskFindAllResponse")
    @WebResult(name = "return", targetNamespace = "")
    java.util.List<ru.kozyrev.tm.api.endpoint.TaskDTO> taskFindAll(
            @WebParam(name = "sessionBASE64", targetNamespace = "")
                    java.lang.String sessionBASE64,
            @WebParam(name = "arg1", targetNamespace = "")
                    ru.kozyrev.tm.api.endpoint.Column arg1,
            @WebParam(name = "arg2", targetNamespace = "")
                    ru.kozyrev.tm.api.endpoint.Direction arg2
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskRemoveRequest", output = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskRemoveResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.kozyrev.ru/ITaskEndpoint/taskRemove/Fault/Exception")})
    @RequestWrapper(localName = "taskRemove", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskRemove")
    @ResponseWrapper(localName = "taskRemoveResponse", targetNamespace = "http://endpoint.api.tm.kozyrev.ru/", className = "ru.kozyrev.tm.api.endpoint.TaskRemoveResponse")
    void taskRemove(
            @WebParam(name = "sessionBASE64", targetNamespace = "")
                    java.lang.String sessionBASE64,
            @WebParam(name = "taskId", targetNamespace = "")
                    java.lang.String taskId
    ) throws Exception_Exception;
}
