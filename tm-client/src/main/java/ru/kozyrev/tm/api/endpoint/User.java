
package ru.kozyrev.tm.api.endpoint;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for user complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="user"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://endpoint.api.tm.kozyrev.ru/}abstractEntity"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="passwordHash" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="projects" type="{http://endpoint.api.tm.kozyrev.ru/}project" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="roleType" type="{http://endpoint.api.tm.kozyrev.ru/}roleType" minOccurs="0"/&gt;
 *         &lt;element name="session" type="{http://endpoint.api.tm.kozyrev.ru/}session" minOccurs="0"/&gt;
 *         &lt;element name="tasks" type="{http://endpoint.api.tm.kozyrev.ru/}task" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "user", propOrder = {
        "login",
        "passwordHash",
        "projects",
        "roleType",
        "session",
        "tasks"
})
public class User
        extends AbstractEntity {

    protected String login;
    protected String passwordHash;
    @XmlElement(nillable = true)
    protected List<Project> projects;
    @XmlSchemaType(name = "string")
    protected RoleType roleType;
    protected Session session;
    @XmlElement(nillable = true)
    protected List<Task> tasks;

    /**
     * Gets the value of the login property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the value of the login property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLogin(final String value) {
        this.login = value;
    }

    /**
     * Gets the value of the passwordHash property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * Sets the value of the passwordHash property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPasswordHash(final String value) {
        this.passwordHash = value;
    }

    /**
     * Gets the value of the projects property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the projects property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProjects().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Project }
     *
     *
     */
    public List<Project> getProjects() {
        if (projects == null) {
            projects = new ArrayList<Project>();
        }
        return this.projects;
    }

    /**
     * Gets the value of the roleType property.
     *
     * @return
     *     possible object is
     *     {@link RoleType }
     *
     */
    public RoleType getRoleType() {
        return roleType;
    }

    /**
     * Sets the value of the roleType property.
     *
     * @param value
     *     allowed object is
     *     {@link RoleType }
     *
     */
    public void setRoleType(final RoleType value) {
        this.roleType = value;
    }

    /**
     * Gets the value of the session property.
     *
     * @return
     *     possible object is
     *     {@link Session }
     *
     */
    public Session getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     *
     * @param value
     *     allowed object is
     *     {@link Session }
     *
     */
    public void setSession(final Session value) {
        this.session = value;
    }

    /**
     * Gets the value of the tasks property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tasks property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTasks().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Task }
     *
     *
     */
    public List<Task> getTasks() {
        if (tasks == null) {
            tasks = new ArrayList<Task>();
        }
        return this.tasks;
    }

}
