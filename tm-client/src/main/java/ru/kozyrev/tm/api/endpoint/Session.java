
package ru.kozyrev.tm.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for session complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="session"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://endpoint.api.tm.kozyrev.ru/}abstractEntity"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="roleType" type="{http://endpoint.api.tm.kozyrev.ru/}roleType" minOccurs="0"/&gt;
 *         &lt;element name="signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="timestamp" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="user" type="{http://endpoint.api.tm.kozyrev.ru/}user" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "session", propOrder = {
        "roleType",
        "signature",
        "timestamp",
        "user"
})
public class Session
        extends AbstractEntity {

    @XmlSchemaType(name = "string")
    protected RoleType roleType;
    protected String signature;
    protected Long timestamp;
    protected User user;

    /**
     * Gets the value of the roleType property.
     *
     * @return
     *     possible object is
     *     {@link RoleType }
     *
     */
    public RoleType getRoleType() {
        return roleType;
    }

    /**
     * Sets the value of the roleType property.
     *
     * @param value
     *     allowed object is
     *     {@link RoleType }
     *
     */
    public void setRoleType(final RoleType value) {
        this.roleType = value;
    }

    /**
     * Gets the value of the signature property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSignature() {
        return signature;
    }

    /**
     * Sets the value of the signature property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSignature(final String value) {
        this.signature = value;
    }

    /**
     * Gets the value of the timestamp property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    public Long getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setTimestamp(final Long value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the user property.
     *
     * @return
     *     possible object is
     *     {@link User }
     *
     */
    public User getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     *
     * @param value
     *     allowed object is
     *     {@link User }
     *
     */
    public void setUser(final User value) {
        this.user = value;
    }

}
