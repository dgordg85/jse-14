package ru.kozyrev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.*;
import ru.kozyrev.tm.api.service.EndpointLocator;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.command.AccessForbiddenException;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.util.DateUtil;
import ru.kozyrev.tm.util.StringUtil;

import java.lang.Exception;
import java.util.List;


@Getter
@Setter
public final class StateService {
    @NotNull
    private EndpointLocator endpointLocator;

    @Nullable
    private String session = null;

    @Nullable
    private String login = null;

    @Nullable
    private ru.kozyrev.tm.api.endpoint.RoleType roleType = null;

    @Nullable
    private String userId = null;

    @NotNull
    private Column projectColumn = Column.NUM;

    @NotNull
    private Column taskColumn = Column.NUM;

    @NotNull
    private Direction projectDirection = Direction.ASC;

    @NotNull
    private Direction taskDirection = Direction.ASC;

    @Nullable
    private List<TaskDTO> listTasks = null;

    @Nullable
    private List<ProjectDTO> listProjects = null;

    public final void clearSession() {
        session = null;
        login = null;
        roleType = null;
        userId = null;
        projectColumn = Column.NUM;
        taskColumn = Column.NUM;
        projectDirection = Direction.ASC;
        taskDirection = Direction.ASC;
        listTasks = null;
        listProjects = null;
    }

    public void clearTasks() {
        listTasks = null;
    }

    public void clearProjects() {
        listProjects = null;
    }

    @NotNull
    public final String getRoleStr() {
        if (roleType == null) {
            return "";
        }
        return RoleType.valueOf(roleType.value()).getDisplayName();
    }

    @NotNull
    public UserDTO getUserDTO() {
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(login);
        userDTO.setRoleType(roleType);
        userDTO.setId(userId);
        return userDTO;
    }

    @NotNull
    public String getTaskId(@Nullable final String num) throws Exception {
        return getTaskDTO(num).getId();
    }

    @NotNull
    public String getProjectId(@Nullable final String num) throws Exception {
        return getProjectDTO(num).getId();
    }

    @NotNull
    public TaskDTO getTaskDTO(@Nullable final String num) throws Exception {
        if (num == null || num.isEmpty()) {
            throw new IndexException();
        }
        if (listTasks == null) {
            throw new AccessForbiddenException();
        }
        @NotNull final int id = StringUtil.parseToInt(num);
        @NotNull final TaskDTO taskDTO;
        try {
            taskDTO = listTasks.get(id);
        } catch (final IndexOutOfBoundsException e) {
            throw new IndexException();
        }
        return taskDTO;
    }

    @NotNull
    public ProjectDTO getProjectDTO(@Nullable final String num) throws Exception {
        if (num == null || num.isEmpty()) {
            throw new IndexException();
        }
        if (listProjects == null) {
            throw new AccessForbiddenException();
        }
        @NotNull final int id = StringUtil.parseToInt(num);
        @NotNull final ProjectDTO projectDTO;
        try {
            projectDTO = listProjects.get(id);
        } catch (final IndexOutOfBoundsException e) {
            throw new IndexException();
        }
        return projectDTO;
    }

    public final void printTasks(@Nullable final String projectId) throws Exception {
        @Nullable final List<TaskDTO> list = listTasks;
        if (list == null || list.size() == 0) {
            System.out.println("[EMPTY]");
            return;
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        int count = 1;
        for (int i = 0; i < list.size(); i++) {
            if (!projectId.equals(list.get(i).getProjectId())) {
                continue;
            }
            System.out.printf("%d. %s, %s, EDIT#%d, s:%s, f:%s - %s\n",
                    count++,
                    list.get(i).getName(),
                    list.get(i).getDescription(),
                    i,
                    DateUtil.getDateXML(list.get(i).getDateStart()),
                    DateUtil.getDateXML(list.get(i).getDateFinish()),
                    printStatus(list.get(i).getStatus())
            );
        }
    }

    public final <T extends AbstractObjectDTO> void printList(@Nullable final List<T> list) {
        if (list == null || list.size() == 0) {
            System.out.println("[EMPTY]");
            return;
        }
        int count = 1;
        for (int i = 0; i < list.size(); i++) {
            System.out.printf("%d. %s, %s, EDIT#%d, s:%s, f:%s - %s\n",
                    count++,
                    list.get(i).getName(),
                    list.get(i).getDescription(),
                    i,
                    DateUtil.getDateXML(list.get(i).getDateStart()),
                    DateUtil.getDateXML(list.get(i).getDateFinish()),
                    printStatus(list.get(i).getStatus())
            );
        }
    }

    private String printStatus(@NotNull final DocumentStatus status) {
        @NotNull final String statusStr = status.value();
        return ru.kozyrev.tm.enumerated.DocumentStatus.valueOf(statusStr).getDisplayName();
    }

    public void updateProjects(@NotNull final String session) throws Exception {
        @NotNull final List<ProjectDTO> list = endpointLocator.getProjectEndpoint().projectFindAll(session, projectColumn, projectDirection);
        setListProjects(list);
    }

    public void updateTasks(@NotNull final String session) throws Exception {
        @NotNull final List<TaskDTO> list = endpointLocator.getTaskEndpoint().taskFindAll(session, taskColumn, taskDirection);
        setListTasks(list);
    }

    public void setEndpointLocator(@NotNull final EndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
    }
}
