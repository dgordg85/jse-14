package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.ProjectDTO;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.util.DateUtil;

public final class ProjectCreateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 22;

    public ProjectCreateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Create new project.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(stateService.getUserId());

        System.out.println("[PROJECT CREATE]\nENTER NAME:");
        projectDTO.setName(terminalService.nextLine());

        System.out.println("ENTER DESCRIPTION:");
        projectDTO.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATE START:");
        projectDTO.setDateStart(DateUtil.parseDateXML(terminalService.nextLine()));

        System.out.println("ENTER DATE FINISH:");
        projectDTO.setDateFinish(DateUtil.parseDateXML(terminalService.nextLine()));

        projectEndpoint.projectPersist(session, projectDTO);
        System.out.println("[DONE]");

        stateService.updateProjects(session);
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
