package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;

public final class TaskListCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 40;

    public TaskListCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("INPUT ID PROJECT");
        @NotNull final String projectNum = terminalService.nextLine();
        @NotNull final String id = stateService.getProjectId(projectNum);

        System.out.println("[TASKS LIST OF PROJECT]");
        stateService.printTasks(id);
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
