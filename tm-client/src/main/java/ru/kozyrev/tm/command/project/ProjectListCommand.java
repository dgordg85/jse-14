package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;

public final class ProjectListCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 20;

    public ProjectListCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show all projects.";
    }

    @Override
    public final void execute() {
        System.out.println("[PROJECTS LIST]");
        stateService.printList(stateService.getListProjects());
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
