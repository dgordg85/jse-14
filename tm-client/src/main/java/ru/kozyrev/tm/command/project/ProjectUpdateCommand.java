package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.DocumentStatus;
import ru.kozyrev.tm.api.endpoint.ProjectDTO;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.util.DateUtil;

public final class ProjectUpdateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 24;

    public ProjectUpdateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Update Selected project.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        @Nullable final ProjectDTO projectDTO;

        System.out.println("[UPDATE PROJECT]\nENTER ID:");
        @NotNull final String projectNum = terminalService.nextLine();
        projectDTO = stateService.getProjectDTO(projectNum);

        System.out.println("ENTER NAME:");
        projectDTO.setName(terminalService.nextLine());

        System.out.println("ENTER DESCRIPTION:");
        projectDTO.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATE START:");
        @NotNull final String dateStart = terminalService.nextLine();
        if (!dateStart.isEmpty()) {
            projectDTO.setDateStart(DateUtil.parseDateXML(dateStart));
        }

        System.out.println("ENTER DATE FINISH:");
        @NotNull final String dateFinish = terminalService.nextLine();
        if (!dateFinish.isEmpty()) {
            projectDTO.setDateFinish(DateUtil.parseDateXML(dateFinish));
        }

        System.out.println("STATUS: (plan / progress / ready)");
        @NotNull final String statusString = terminalService.nextLine();
        if (!statusString.isEmpty()) {
            @NotNull final String status = ru.kozyrev.tm.enumerated.DocumentStatus.getType(statusString);
            projectDTO.setStatus(DocumentStatus.fromValue(status));
        }

        projectEndpoint.projectMerge(session, projectDTO);
        System.out.println("[OK]");

        stateService.updateProjects(session);
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
