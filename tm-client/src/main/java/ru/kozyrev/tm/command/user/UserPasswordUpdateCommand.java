package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.api.endpoint.UserDTO;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.exception.user.UserPasswordEmptyException;
import ru.kozyrev.tm.exception.user.UserPasswordMatchException;
import ru.kozyrev.tm.util.HashUtil;

public final class UserPasswordUpdateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 12;

    public UserPasswordUpdateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-pass-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Use for updating password";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(stateService.getUserId());

        System.out.println("[UPDATE PASSWORD]");

        System.out.println("ENTER CURRENT PASSWORD:");
        @NotNull final String hashPassword = HashUtil.getHash(terminalService.nextLine());

        System.out.println("ENTER NEW PASSWORD:");
        userDTO.setPasswordHash(HashUtil.getHash(terminalService.nextLine()));

        System.out.println("RE-ENTER PASSWORD:");
        @NotNull final String hashReNewPassword = HashUtil.getHash(terminalService.nextLine());

        if (!hashReNewPassword.equals(userDTO.getPasswordHash())) {
            throw new UserPasswordMatchException();
        }
        if (hashReNewPassword.isEmpty()) {
            throw new UserPasswordEmptyException();
        }
        userEndpoint.userUpdatePassword(session, userDTO, hashPassword);

        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
