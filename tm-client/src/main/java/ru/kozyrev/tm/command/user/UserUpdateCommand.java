package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.api.endpoint.UserDTO;
import ru.kozyrev.tm.command.AbstractCommand;

public final class UserUpdateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 9;

    public UserUpdateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Update user login.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(stateService.getUserId());
        System.out.println("[UPDATE USER]");

        System.out.println("ENTER NEW LOGIN:");
        userDTO.setLogin(terminalService.nextLine());

        userEndpoint.userMerge(session, userDTO);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
