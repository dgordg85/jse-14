package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.DocumentStatus;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.api.endpoint.TaskDTO;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.util.DateUtil;

public final class TaskUpdateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 46;

    public TaskUpdateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Update selected task.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        @Nullable final TaskDTO taskDTO;

        System.out.println("[UPDATE TASK]\nENTER ID:");
        @NotNull final String taskNum = terminalService.nextLine();
        taskDTO = stateService.getTaskDTO(taskNum);

        System.out.println("NEW NAME:");
        taskDTO.setName(terminalService.nextLine());

        System.out.println("NEW PROJECT ID:");
        @NotNull final String projectNum = terminalService.nextLine();
        taskDTO.setProjectId(stateService.getProjectId(projectNum));

        System.out.println("ENTER DESCRIPTION:");
        taskDTO.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATE START:");
        @NotNull final String dateBegin = terminalService.nextLine();
        if (!dateBegin.isEmpty()) {
            taskDTO.setDateStart(DateUtil.parseDateXML(dateBegin));
        }

        System.out.println("ENTER DATE FINISH:");
        @NotNull final String dateFinish = terminalService.nextLine();
        if (!dateFinish.isEmpty()) {
            taskDTO.setDateFinish(DateUtil.parseDateXML(dateFinish));
        }

        System.out.println("STATUS: (plan / progress / ready)");
        @NotNull final String statusString = terminalService.nextLine();
        if (!statusString.isEmpty()) {
            taskDTO.setStatus(DocumentStatus.valueOf(statusString));
        }
        taskEndpoint.taskMerge(session, taskDTO);
        System.out.println("[OK]");

        stateService.updateTasks(session);
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }

}
