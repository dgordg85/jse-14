package ru.kozyrev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.ProjectDTO;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.api.endpoint.TaskDTO;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.exception.entity.EmptyStringException;

import java.util.List;

public final class TextFinderCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 95;

    public TextFinderCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "find";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Search words in Project/Tasks";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        System.out.println("[FIND]");
        System.out.println("Print word...");
        @NotNull final String word = terminalService.nextLine();
        if (word.isEmpty()) {
            throw new EmptyStringException();
        }

        @Nullable final List<ProjectDTO> listProjects = projectEndpoint.projectFindWord(session, word);
        if (listProjects != null && listProjects.size() != 0) {
            System.out.println("[FIND PROJECTS]");
            stateService.printList(listProjects);
        }

        @Nullable final List<TaskDTO> listTasks = taskEndpoint.taskFindWord(session, word);
        if (listTasks != null && listTasks.size() != 0) {
            System.out.println("[FIND TASKS]");
            stateService.printList(listTasks);
        }
        if (listProjects != null && listTasks != null && listProjects.size() == 0 && listTasks.size() == 0) {
            System.out.printf("%s - not find!\n", word);
        }
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
