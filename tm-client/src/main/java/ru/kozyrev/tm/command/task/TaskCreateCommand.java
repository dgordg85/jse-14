package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.api.endpoint.TaskDTO;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.util.DateUtil;

public final class TaskCreateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 44;

    public TaskCreateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Create new tasks.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(stateService.getUserId());

        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectNum = terminalService.nextLine();
        taskDTO.setProjectId(stateService.getProjectId(projectNum));

        System.out.println("[TASK CREATE]\nENTER NAME:");
        taskDTO.setName(terminalService.nextLine());

        System.out.println("ENTER DESCRIPTION:");
        taskDTO.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATE START:");
        taskDTO.setDateStart(DateUtil.parseDateXML(terminalService.nextLine()));

        System.out.println("ENTER DATE FINISH:");
        taskDTO.setDateFinish(DateUtil.parseDateXML(terminalService.nextLine()));

        taskEndpoint.taskPersist(session, taskDTO);
        System.out.println("[OK]");

        stateService.updateTasks(session);
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
