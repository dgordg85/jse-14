package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.api.endpoint.UserDTO;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.util.HashUtil;

public final class UserLoginCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 3;

    public UserLoginCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        secure = true;
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Use for logging in system.";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOGIN]");

        System.out.println("ENTER LOGIN:");
        @NotNull final String login = terminalService.nextLine();

        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = terminalService.nextLine();

        @Nullable final String session = sessionEndpoint.openSession(login, HashUtil.getHash(password));
        if (session != null) {
            stateService.setSession(session);
            @NotNull final UserDTO user = userEndpoint.userFindOneByLogin(session, login);
            stateService.setLogin(user.getLogin());
            stateService.setRoleType(user.getRoleType());
            stateService.setUserId(user.getId());
            stateService.updateProjects(session);
            stateService.updateTasks(session);
            System.out.println("[OK]");
        } else {
            System.out.println("LOGIN FAILED!");
        }
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
