package ru.kozyrev.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.endpoint.*;
import ru.kozyrev.tm.api.service.EndpointLocator;
import ru.kozyrev.tm.api.service.ITerminalService;
import ru.kozyrev.tm.service.StateService;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    protected Boolean secure = false;

    @NotNull
    protected IProjectEndpoint projectEndpoint;

    @NotNull
    protected ITaskEndpoint taskEndpoint;

    @NotNull
    protected IUserEndpoint userEndpoint;

    @NotNull
    protected IAdminEndpoint adminEndpoint;

    @NotNull
    protected ISessionEndpoint sessionEndpoint;

    @NotNull
    protected ITerminalService terminalService;

    @NotNull
    protected StateService stateService;

    public final void setEndpointLocator(@NotNull final EndpointLocator endpointLocator) {
        this.projectEndpoint = endpointLocator.getProjectEndpoint();
        this.taskEndpoint = endpointLocator.getTaskEndpoint();
        this.userEndpoint = endpointLocator.getUserEndpoint();
        this.adminEndpoint = endpointLocator.getAdminEndpoint();
        this.sessionEndpoint = endpointLocator.getSessionEndpoint();
    }

    public final void setTerminalService(@NotNull final ITerminalService terminalService) {
        this.terminalService = terminalService;
    }

    public final void setStateService(@NotNull final StateService stateService) {
        this.stateService = stateService;
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract Integer getSortId();
}
