package ru.kozyrev.tm.exception.command;

public final class CommandException extends Exception {
    public CommandException() {
        super("Wrong command! User 'help' command!");
    }
}
