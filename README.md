**РАЗРАБОТКА КОНСОЛЬНОГО ПРИЛОЖЕНИЯ**
===================================

GITLAB URL
-----------------------------------

[https://gitlab.com/dgordg85/jse-14](https://gitlab.com/dgordg85/jse-14 "GITLAB")

ТРЕБОВАНИЯ К SOFTWARE
-----------------------------------
* JRE

ОПИСАНИЕ СТЕКА ТЕХНОЛОГИЙ
-----------------------------------
* JDK 1.8
* MAVEN
* MySQL 5.5
* JCabi Manifests
* Lombok
* FasterXML
* JaxB
* JAX-WS (SOAP)
* MyBatis
* Junit 5

ИМЯ РАЗРАБОТЧИКА И КОНТАКТЫ
-----------------------------------
    Александр К.
    Skype: roverc0m
    E-mail: dgordg85@gmail.com

КОМАНДЫ ДЛЯ СБОРКИ ПРИЛОЖЕНИЯ
-----------------------------------
    git clone http://gitlab.volnenko.school/Kozyrev/jse-14.git
    cd jse-14
    mvn clean install
    
КОМАНДА ДЛЯ ЗАПУСКА СЕРВЕРА
-----------------------------------
    java -jar tm-server/target/task-manager-server/bin/tm_server-2.2.jar
    
КОМАНДА ДЛЯ ЗАПУСКА КЛИЕНТА
-----------------------------------
    java -jar tm-client/target/task-manager-client/bin/tm_clients-2.2.jar
    
ДОКУМЕНТАЦИЯ SOAP
-----------------------------------
    tm-server/target/docs/apidocs/index.html
