package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.ITaskRepository;
import ru.kozyrev.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;

public interface ITaskService extends IObjectService<Task> {
    @Nullable
    List<Task> findAll() throws Exception;

    void removeAll() throws Exception;

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);

    @Nullable
    Task findOne(@Nullable String id, @Nullable final String userId) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable String userId) throws Exception;

    void remove(@Nullable String id, @Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable
    List<Task> findWord(@Nullable String word, @Nullable String userId) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable String projectId, @Nullable String userId) throws Exception;

    void removeAll(@Nullable String projectId, @Nullable String userId) throws Exception;

    @NotNull
    ITaskRepository getRepository(@NotNull EntityManager em);
}
