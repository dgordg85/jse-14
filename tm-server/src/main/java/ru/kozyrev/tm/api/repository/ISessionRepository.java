package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {
    void persist(@NotNull Session session);

    void merge(@NotNull Session session);

    @Nullable
    Session findOne(@NotNull String id) throws Exception;

    @Nullable
    Session findOneByUserId(@NotNull String userId) throws Exception;

    @NotNull
    List<Session> findAll() throws Exception;

    void remove(@NotNull Session session);
}
