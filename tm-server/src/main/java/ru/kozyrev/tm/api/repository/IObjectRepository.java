package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IObjectRepository<T> extends IRepository<T> {
    @Nullable
    T findOne(@NotNull String id, @NotNull String userId) throws Exception;

    @NotNull
    List<T> findAllByUserId(@NotNull String userId) throws Exception;

    @NotNull
    List<T> findWord(@NotNull String word, @NotNull String userId) throws Exception;

    @NotNull
    List<T> findAllByNum(@NotNull final String userId);

    @NotNull
    List<T> findAllByDateStart(@NotNull final String userId);

    @NotNull
    List<T> findAllByDateFinish(@NotNull final String userId);

    @NotNull
    List<T> findAllByStatus(@NotNull final String userId);

    @NotNull
    List<T> findAllByNumDesc(@NotNull final String userId);

    @NotNull
    List<T> findAllByDateStartDesc(@NotNull final String userId);

    @NotNull
    List<T> findAllByDateFinishDesc(@NotNull final String userId);

    @NotNull
    List<T> findAllByStatusDesc(@NotNull final String userId);
}
