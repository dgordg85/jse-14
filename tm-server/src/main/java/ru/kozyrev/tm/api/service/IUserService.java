package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IUserRepository;
import ru.kozyrev.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

public interface IUserService extends IEntityService<User> {
    @Nullable
    List<User> findAll() throws Exception;

    void removeAll() throws Exception;

    @Nullable
    User findOne(@Nullable String id) throws Exception;

    void persist(@Nullable User user) throws Exception;

    void persistSaltPass(@Nullable User user) throws Exception;

    void merge(@Nullable User user) throws Exception;

    void mergeSaltPass(@Nullable User user) throws Exception;

    void remove(@Nullable String id) throws Exception;

    void persist(@Nullable List<User> users) throws Exception;

    @Nullable
    User findOneByLogin(@Nullable String login) throws Exception;

    void updateUserPassword(
            @Nullable User user,
            @Nullable String checkPassword
    ) throws Exception;

    @NotNull
    IUserRepository getRepository(@NotNull EntityManager em);
}
