package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IObjectRepository;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import javax.persistence.EntityManager;
import java.util.List;

public interface IObjectService<T> extends IService<T> {
    void persist(@Nullable T object) throws Exception;

    @Nullable
    List<T> findAll() throws Exception;

    void removeAll() throws Exception;

    void persist(@Nullable List<T> object) throws Exception;

    @Nullable
    T findOne(@Nullable String id, @Nullable final String userId) throws Exception;

    @Nullable
    List<T> findAll(@Nullable String userId) throws Exception;

    void persist(@Nullable T object, @Nullable String userId) throws Exception;

    void merge(@Nullable T object, @Nullable String userId) throws Exception;

    @NotNull
    List<T> findAll(
            @NotNull final Column column,
            @NotNull final Direction direction,
            @Nullable final String userId
    ) throws Exception;

    void remove(@Nullable String objectId, @Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable
    List<T> findWord(@Nullable String word, @Nullable String userId) throws Exception;

    @NotNull
    IObjectRepository<T> getRepository(@NotNull EntityManager em);
}
