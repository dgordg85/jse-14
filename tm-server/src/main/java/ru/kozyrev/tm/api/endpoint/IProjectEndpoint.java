package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.dto.ProjectDTO;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {
    @Nullable
    @WebMethod
    List<ProjectDTO> projectFindAll(
            @Nullable final @WebParam(name = "sessionBASE64") String sessionBASE64,
            @NotNull final Column column,
            @NotNull final Direction direction
    ) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> projectFindWord(
            @Nullable final @WebParam(name = "sessionBASE64") String sessionBASE64,
            @NotNull final @WebParam(name = "word") String word
    ) throws Exception;

    @WebMethod
    void projectPersist(
            @Nullable final @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable final ProjectDTO projectDTO
    ) throws Exception;

    @WebMethod
    void projectMerge(
            @Nullable final @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable final ProjectDTO projectDTO
    ) throws Exception;

    @WebMethod
    void projectRemove(
            @Nullable final @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable final @WebParam(name = "projectId") String projectId
    ) throws Exception;

    @WebMethod
    void projectRemoveAll(
            @Nullable final @WebParam(name = "sessionBASE64") String sessionBASE64
    ) throws Exception;
}
