package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Project;

public interface IProjectRepository extends IObjectRepository<Project> {
    void persist(@NotNull Project project);

    void merge(@NotNull Project project);

    @Nullable
    Project findOne(@NotNull String id, @NotNull String userId) throws Exception;
}
