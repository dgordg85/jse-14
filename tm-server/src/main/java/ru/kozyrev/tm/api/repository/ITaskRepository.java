package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IObjectRepository<Task> {
    void persist(@NotNull Task task);

    void merge(@NotNull Task task);

    @Nullable
    Task findOne(@NotNull String id, @NotNull String userId) throws Exception;

    @NotNull
    List<Task> findAll() throws Exception;

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId) throws Exception;

    @NotNull
    List<Task> findAllByProjectId(@NotNull String projectId, @NotNull final String userId);

    @NotNull
    List<Task> findWord(@NotNull String word, @NotNull String userId) throws Exception;

    @NotNull
    List<Task> findAllByNum(@NotNull final String userId);

    @NotNull
    List<Task> findAllByDateStart(@NotNull final String userId);

    @NotNull
    List<Task> findAllByDateFinish(@NotNull final String userId);

    @NotNull
    List<Task> findAllByStatus(@NotNull final String userId);

    @NotNull
    List<Task> findAllByNumDesc(@NotNull final String userId);

    @NotNull
    List<Task> findAllByDateStartDesc(@NotNull final String userId);

    @NotNull
    List<Task> findAllByDateFinishDesc(@NotNull final String userId);

    @NotNull
    List<Task> findAllByStatusDesc(@NotNull final String userId);
}
