package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {
    void persist(@NotNull User user);

    void merge(@NotNull User user);

    @Nullable
    User findOne(@NotNull String id) throws Exception;

    @Nullable
    User findOneByLogin(@NotNull String login) throws Exception;

    @NotNull List<User> findAll() throws Exception;

    void remove(@NotNull User user);
}