package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.dto.ProjectDTO;
import ru.kozyrev.tm.dto.TaskDTO;
import ru.kozyrev.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IAdminEndpoint {
    @WebMethod
    void adminBinSave(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void adminBinLoad(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void adminJsonLoadFasterXML(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void adminJsonSaveFasterXML(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void adminXmlLoadFasterXML(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void adminXmlSaveFasterXML(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void adminJsonSaveJaxB(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void adminJsonLoadJaxB(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void adminXmlSaveJaxB(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void adminXmlLoadJaxB(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void projectPersistList(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable List<ProjectDTO> projects
    ) throws Exception;

    @WebMethod
    void projectRemoveAll(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    @Nullable
    List<ProjectDTO> projectFindAll(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void taskPersistList(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable List<TaskDTO> tasks
    ) throws Exception;

    @WebMethod
    @Nullable
    List<TaskDTO> taskFindAll(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void taskRemoveAll(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void userPersistList(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable List<UserDTO> users
    ) throws Exception;

    @WebMethod
    @Nullable
    List<UserDTO> userFindAll(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;


    @WebMethod
    void userRemoveAll(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;
}
