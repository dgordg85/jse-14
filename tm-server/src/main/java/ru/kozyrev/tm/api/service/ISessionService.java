package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.ISessionRepository;
import ru.kozyrev.tm.entity.Session;

import javax.persistence.EntityManager;

public interface ISessionService extends IEntityService<Session> {
    @Nullable
    Session findOne(@Nullable String id) throws Exception;

    void persist(@Nullable Session session) throws Exception;

    @Nullable
    String openSession(@Nullable String login, @Nullable String hashPassword) throws Exception;

    void closeSession(@Nullable Session session) throws Exception;

    boolean isValid(@Nullable Session session, boolean checkForbidden) throws Exception;

    @Nullable
    Session findOneByUserId(@Nullable String userId) throws Exception;

    @NotNull
    ISessionRepository getRepository(@NotNull EntityManager em);

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);
}
