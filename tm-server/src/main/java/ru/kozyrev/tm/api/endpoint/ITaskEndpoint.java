package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.dto.TaskDTO;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {
    @Nullable
    @WebMethod
    List<TaskDTO> taskFindAll(
            @Nullable final @WebParam(name = "sessionBASE64") String sessionBASE64,
            @NotNull final Column column,
            @NotNull final Direction direction
    ) throws Exception;

    @Nullable
    @WebMethod
    List<TaskDTO> taskFindWord(
            @Nullable final @WebParam(name = "sessionBASE64") String sessionBASE64,
            @NotNull final @WebParam(name = "word") String word
    ) throws Exception;

    @WebMethod
    void taskPersist(
            @Nullable final @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable final TaskDTO taskDTO
    ) throws Exception;

    @WebMethod
    void taskMerge(
            @Nullable final @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable final TaskDTO taskDTO
    ) throws Exception;

    @WebMethod
    void taskRemove(
            @Nullable final @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable final @WebParam(name = "taskId") String taskId
    ) throws Exception;

    @WebMethod
    void taskRemoveAll(
            @Nullable final @WebParam(name = "sessionBASE64") String sessionBASE64
    ) throws Exception;

    @WebMethod
    void taskRemoveAllByProject(
            @Nullable final @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable final @WebParam(name = "projectId") String projectId
    ) throws Exception;
}
