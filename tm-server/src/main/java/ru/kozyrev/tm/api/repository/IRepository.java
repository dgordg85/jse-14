package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<T> {
    void persist(@NotNull final T entity);

    void merge(@NotNull final T entity);

    @Nullable
    T findOne(@NotNull String id) throws Exception;

    @NotNull
    List<T> findAll() throws Exception;

    void remove(@NotNull T entity);

    void removeAll(@NotNull final List<T> list);

    @NotNull
    Class<T> getClassName();
}
