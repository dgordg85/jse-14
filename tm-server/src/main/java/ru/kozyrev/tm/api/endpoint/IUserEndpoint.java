package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint {
    @Nullable
    @WebMethod
    UserDTO userFindOne(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "userId") String userId
    ) throws Exception;

    @WebMethod
    void userPersist(@Nullable UserDTO userDTO) throws Exception;

    @WebMethod
    void userMerge(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable UserDTO userDTO
    ) throws Exception;

    @WebMethod
    void userRemove(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "userId") String userId
    ) throws Exception;

    @WebMethod
    @Nullable
    UserDTO userFindOneByLogin(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "login") String login
    ) throws Exception;

    @WebMethod
    void userUpdatePassword(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable UserDTO userDTO,
            @Nullable @WebParam(name = "currentPassword") String checkPassword
    ) throws Exception;
}
