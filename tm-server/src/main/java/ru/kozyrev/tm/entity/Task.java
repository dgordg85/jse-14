package ru.kozyrev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.constant.db.FieldConst;
import ru.kozyrev.tm.constant.db.TableConst;
import ru.kozyrev.tm.dto.TaskDTO;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = TableConst.TASKS)
public final class Task extends AbstractObject implements Serializable {
    private final static long SerialVersionUID = 1L;

    @Nullable
    @ManyToOne
    @JoinColumn(name = FieldConst.PROJECT_ID, referencedColumnName = FieldConst.ID, nullable = false)
    private Project project;

    @Nullable
    public static final TaskDTO getDTO(@Nullable final Task task) {
        if (task == null) {
            return null;
        }
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setProjectId(task.getProject().getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateStart(task.getDateStart());
        taskDTO.setDateFinish(task.getDateFinish());
        taskDTO.setUserId(task.getUser().getId());
        taskDTO.setStatus(task.getStatus());
        return taskDTO;
    }

    @Nullable
    public static final List<TaskDTO> getListDTO(@Nullable final List<Task> list) {
        if (list == null) {
            return null;
        }
        @NotNull final List<TaskDTO> listDTO = new ArrayList<>();
        for (final Task task : list) {
            listDTO.add(getDTO(task));
        }
        return listDTO;
    }
}
