package ru.kozyrev.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.constant.db.FieldConst;
import ru.kozyrev.tm.enumerated.DocumentStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractObject extends AbstractEntity implements Serializable {
    private final static long SerialVersionUID = 1L;

    @NotNull
    @ManyToOne
    protected User user;

    @Nullable
    @Column(name = FieldConst.NAME, nullable = false)
    protected String name;

    @Nullable
    @Column(name = FieldConst.DESCRIPTION, nullable = false)
    protected String description;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @Column(name = FieldConst.DATE_START, nullable = false)
    protected Date dateStart;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @Column(name = FieldConst.DATE_FINISH, nullable = false)
    protected Date dateFinish;

    @Nullable
    @Enumerated(value = EnumType.STRING)
    @Column(name = FieldConst.STATUS, columnDefinition = "ENUM('PLANNED','IN-PROGRESS', 'READY')", nullable = false)
    protected DocumentStatus status = null;

    @Nullable
    @Column(name = FieldConst.TIMESTAMP, nullable = false, length = 20)
    @Type(type = "long")
    private Long timestamp;
}
