package ru.kozyrev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.constant.db.FieldConst;
import ru.kozyrev.tm.constant.db.TableConst;
import ru.kozyrev.tm.dto.SessionDTO;
import ru.kozyrev.tm.enumerated.RoleType;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = TableConst.SESSIONS)
public final class Session extends AbstractEntity implements Serializable {
    @Nullable
    @Column(name = FieldConst.TIMESTAMP, nullable = false, length = 20)
    @Type(type = "long")
    private Long timestamp;

    @JsonIgnore
    @NotNull
    @OneToOne
    @JoinColumn(nullable = false)
    private User user;

    @Nullable
    @Enumerated(value = EnumType.STRING)
    @Column(name = FieldConst.ROLE_TYPE, columnDefinition = "ENUM('USER','ADMIN')", nullable = false)
    private RoleType roleType;

    @JsonIgnore
    @Nullable
    @Column(name = FieldConst.SIGNATURE, nullable = false, length = 32)
    private String signature;

    @Nullable
    public static final SessionDTO getDTO(@Nullable final Session session) {
        if (session == null) {
            return null;
        }
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        sessionDTO.setTimestamp(session.getTimestamp());
        sessionDTO.setRoleType(session.getRoleType());
        sessionDTO.setUserId(session.getUser().getId());
        return sessionDTO;
    }
}
