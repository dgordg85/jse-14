package ru.kozyrev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.constant.db.TableConst;
import ru.kozyrev.tm.dto.ProjectDTO;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = TableConst.PROJECTS)
public final class Project extends AbstractObject implements Serializable {
    private final static long SerialVersionUID = 1L;

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    public static final ProjectDTO getDTO(@Nullable final Project project) {
        if (project == null) {
            return null;
        }
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateStart(project.getDateStart());
        projectDTO.setDateFinish(project.getDateFinish());
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setStatus(project.getStatus());
        return projectDTO;
    }

    @Nullable
    public static final List<ProjectDTO> getListDTO(@Nullable final List<Project> list) {
        if (list == null) {
            return null;
        }
        @NotNull final List<ProjectDTO> listDTO = new ArrayList<>();
        for (final Project task : list) {
            listDTO.add(getDTO(task));
        }
        return listDTO;
    }
}
