package ru.kozyrev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.constant.db.FieldConst;
import ru.kozyrev.tm.constant.db.TableConst;
import ru.kozyrev.tm.dto.UserDTO;
import ru.kozyrev.tm.enumerated.RoleType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = TableConst.USERS)
public final class User extends AbstractEntity implements Serializable {
    @Nullable
    @OneToOne(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Session session;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @Nullable
    @Column(name = FieldConst.LOGIN, nullable = false, unique = true, length = 20)
    private String login;

    @Nullable
    @Column(name = FieldConst.PASSWORD_HASH, nullable = false, length = 32)
    private String passwordHash;

    @Nullable
    @Enumerated(value = EnumType.STRING)
    @Column(name = FieldConst.ROLE_TYPE, columnDefinition = "ENUM('USER','ADMIN')", nullable = false)
    private RoleType roleType = null;

    @Nullable
    public static final UserDTO getDTO(@Nullable final User user) {
        if (user == null) {
            return null;
        }
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setRoleType(user.getRoleType());
        return userDTO;
    }

    @Nullable
    public static final List<UserDTO> getListDTO(@Nullable final List<User> list) {
        if (list == null) {
            return null;
        }
        @NotNull final List<UserDTO> listDTO = new ArrayList<>();
        for (final User user : list) {
            listDTO.add(getDTO(user));
        }
        return listDTO;
    }
}
