package ru.kozyrev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.constant.db.FieldConst;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {
    private final static long SerialVersionUID = 1L;

    @Id
    @Nullable
    @Column(name = FieldConst.ID, nullable = false, length = 36)
    protected String id;
}
