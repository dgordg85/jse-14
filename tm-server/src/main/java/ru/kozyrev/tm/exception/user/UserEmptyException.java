package ru.kozyrev.tm.exception.user;

public final class UserEmptyException extends Exception {
    public UserEmptyException() {
        super("No user id");
    }
}
