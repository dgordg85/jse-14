package ru.kozyrev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.session.SessionNotValidException;
import ru.kozyrev.tm.util.SignatureUtil;

@Getter
@Setter
public final class SessionDTO extends AbstractEntityDTO {
    @Nullable
    private Long timestamp;

    @Nullable
    private String userId;

    @Nullable
    private RoleType roleType;

    public static final Session getSession(
            @Nullable final SessionDTO sessionDTO,
            @NotNull final ServiceLocator serviceLocator
    ) {
        if (sessionDTO == null) {
            return null;
        }
        @NotNull Session session = new Session();
        session.setId(sessionDTO.id);
        session.setTimestamp(sessionDTO.timestamp);
        session.setRoleType(sessionDTO.roleType);
        try {
            @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.userId);
            if (user == null) {
                throw new SessionNotValidException();
            }
            session.setUser(user);
            session = SignatureUtil.signSession(session);
        } catch (final Exception e) {
            return null;
        }
        return session;
    }
}
