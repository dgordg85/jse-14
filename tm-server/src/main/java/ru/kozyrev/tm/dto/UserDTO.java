package ru.kozyrev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public final class UserDTO extends AbstractEntityDTO implements Serializable {
    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @Nullable
    private RoleType roleType = null;

    @Nullable
    public static final User getUser(@Nullable final UserDTO userDTO, @NotNull final ServiceLocator serviceLocator) {
        if (userDTO == null) {
            return null;
        }
        @NotNull final User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPasswordHash(userDTO.getPasswordHash());
        user.setRoleType(userDTO.getRoleType());
        try {
            @Nullable final List<Project> projectList =
                    serviceLocator.getProjectService().findAll(userDTO.getId());
            if (projectList != null) {
                user.setProjects(projectList);
            }
            @Nullable final List<Task> taskList =
                    serviceLocator.getTaskService().findAll(userDTO.getId());
            if (taskList != null) {
                user.setTasks(taskList);
            }
            @Nullable final Session session = serviceLocator.getSessionService().findOneByUserId(userDTO.getId());
            user.setSession(session);
        } catch (final Exception e) {
            return null;
        }
        return user;
    }

    @Nullable
    public static final List<User> getList(
            @Nullable final List<UserDTO> listUsersDTO,
            @NotNull final ServiceLocator serviceLocator
    ) {
        if (listUsersDTO == null) {
            return null;
        }
        @NotNull final List<User> list = new ArrayList<>();
        for (final UserDTO userDTO : listUsersDTO) {
            list.add(getUser(userDTO, serviceLocator));
        }
        return list;
    }
}
