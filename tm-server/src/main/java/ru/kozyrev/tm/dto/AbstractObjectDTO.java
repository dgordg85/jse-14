package ru.kozyrev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.enumerated.DocumentStatus;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public abstract class AbstractObjectDTO extends AbstractEntityDTO implements Serializable {
    private final static long SerialVersionUID = 1L;

    @Nullable
    protected String userId;

    @Nullable
    protected String name;

    @Nullable
    protected String description;

    @Nullable
    protected Date dateStart;

    @Nullable
    protected Date dateFinish;

    @Nullable
    protected DocumentStatus status = null;
}
