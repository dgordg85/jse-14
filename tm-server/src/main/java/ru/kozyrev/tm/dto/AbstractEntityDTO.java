package ru.kozyrev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
public abstract class AbstractEntityDTO implements Serializable {
    private final static long SerialVersionUID = 1L;

    @Nullable
    protected String id;
}
