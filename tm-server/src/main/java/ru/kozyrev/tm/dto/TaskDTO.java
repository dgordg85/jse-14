package ru.kozyrev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.entity.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public final class TaskDTO extends AbstractObjectDTO implements Serializable {
    private final static long SerialVersionUID = 1L;

    @Nullable
    private String projectId;

    public static final Task getTask(
            @Nullable final TaskDTO taskDTO,
            @NotNull final ServiceLocator serviceLocator
    ) {
        if (taskDTO == null) {
            return null;
        }
        @NotNull final Task task = new Task();
        task.setId(taskDTO.id);
        task.setStatus(taskDTO.status);
        task.setDateStart(taskDTO.dateStart);
        task.setDateFinish(taskDTO.dateFinish);
        task.setName(taskDTO.name);
        task.setDescription(taskDTO.description);
        try {
            @Nullable final Project project =
                    serviceLocator.getProjectService().findOne(taskDTO.projectId, taskDTO.userId);
            if (project == null) {
                return null;
            }
            task.setProject(project);

            @Nullable final User user = serviceLocator.getUserService().findOne(taskDTO.userId);
            if (user == null) {
                return null;
            }
            task.setUser(user);
        } catch (final Exception e) {
            return null;
        }
        return task;
    }

    @Nullable
    public static final List<Task> getList(
            @Nullable final List<TaskDTO> listTasksDTO,
            @NotNull final ServiceLocator serviceLocator
    ) {
        if (listTasksDTO == null) {
            return null;
        }
        @NotNull final List<Task> list = new ArrayList<>();
        for (final TaskDTO taskDTO : listTasksDTO) {
            list.add(getTask(taskDTO, serviceLocator));
        }
        return list;
    }
}
