package ru.kozyrev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public final class ProjectDTO extends AbstractObjectDTO implements Serializable {
    private final static long SerialVersionUID = 1L;


    @Nullable
    public static final Project getProject(
            @Nullable final ProjectDTO projectDTO,
            @NotNull final ServiceLocator serviceLocator
    ) {
        if (projectDTO == null) {
            return null;
        }
        @NotNull final Project project = new Project();
        project.setId(projectDTO.id);
        project.setStatus(projectDTO.status);
        project.setDateStart(projectDTO.dateStart);
        project.setDateFinish(projectDTO.dateFinish);
        project.setName(projectDTO.name);
        project.setDescription(projectDTO.description);
        try {
            if (projectDTO.id != null && !projectDTO.id.isEmpty()) {
                @Nullable final List<Task> list = serviceLocator.getTaskService().findAll(projectDTO.id, projectDTO.userId);
                if (list != null) {
                    project.setTasks(list);
                }
            }
        } catch (final Exception e) {
            project.setId(null);
        }

        try {
            project.setUser(serviceLocator.getUserService().findOne(projectDTO.userId));
        } catch (final Exception e) {
            return null;
        }
        return project;
    }

    @Nullable
    public static final List<Project> getList(
            @Nullable final List<ProjectDTO> listProjectsDTO,
            @NotNull final ServiceLocator serviceLocator
    ) {
        if (listProjectsDTO == null) {
            return null;
        }
        @NotNull final List<Project> list = new ArrayList<>();
        for (final ProjectDTO projectDTO : listProjectsDTO) {
            list.add(getProject(projectDTO, serviceLocator));
        }
        return list;
    }
}
