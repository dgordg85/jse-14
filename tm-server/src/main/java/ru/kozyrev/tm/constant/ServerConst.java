package ru.kozyrev.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class ServerConst {
    @NotNull
    public static final String SALT = "K6@8ll&stv0iej";

    @NotNull
    public static final Integer CYCLE = 127;

    @NotNull
    public static final Long SESSION_LIFETIME = 3600000L; //1 hour

    @NotNull
    public static final String SECRET_KEY = "#aY9DD77w9nG0WY3z$xMdAHub$8V2!3XC1qYs0TUlKoQmT";

    @NotNull
    public final static String PROJECT_URL = "http://localhost:8080/projectService?wsdl";

    @NotNull
    public final static String TASK_URL = "http://localhost:8080/taskService?wsdl";

    @NotNull
    public final static String SESSION_URL = "http://localhost:8080/sessionService?wsdl";

    @NotNull
    public final static String ADMIN_URL = "http://localhost:8080/adminService?wsdl";

    @NotNull
    public final static String USER_URL = "http://localhost:8080/userService?wsdl";


}
