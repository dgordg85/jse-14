package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.dto.SessionDTO;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.exception.session.SessionNotValidException;
import ru.kozyrev.tm.util.AesUtil;

@NoArgsConstructor
public abstract class AbstractEndpoint {
    @NotNull
    protected ServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected final Session checkSession(@Nullable final String sessionBASE64, final boolean checkForbidden) throws Exception {
        @Nullable final SessionDTO sessionDTO = AesUtil.decryptSession(sessionBASE64);
        @Nullable final Session session = SessionDTO.getSession(sessionDTO, serviceLocator);
        final boolean isSessionValid = serviceLocator.getSessionService().isValid(session, checkForbidden);
        if (!isSessionValid) {
            throw new SessionNotValidException();
        }
        return session;
    }
}
