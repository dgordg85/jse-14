package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.IAdminEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.constant.ServerConst;
import ru.kozyrev.tm.dto.ProjectDTO;
import ru.kozyrev.tm.dto.TaskDTO;
import ru.kozyrev.tm.dto.UserDTO;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.entity.User;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kozyrev.tm.api.endpoint.IAdminEndpoint")
@NoArgsConstructor
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {
    @NotNull
    public final static String URL = ServerConst.ADMIN_URL;

    public AdminEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void adminBinSave(@Nullable final String sessionBASE64) throws Exception {
        checkSession(sessionBASE64, true);
        serviceLocator.getAdminService().binSave();
    }

    @Override
    public void adminBinLoad(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, true);
        serviceLocator.getAdminService().binLoad(session);
    }

    @Override
    public void adminJsonLoadFasterXML(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, true);
        serviceLocator.getAdminService().jsonLoadFasterXML(session);
    }

    @Override
    public void adminJsonSaveFasterXML(@Nullable final String sessionBASE64) throws Exception {
        checkSession(sessionBASE64, true);
        serviceLocator.getAdminService().jsonSaveFasterXML();
    }

    @Override
    public void adminXmlLoadFasterXML(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, true);
        serviceLocator.getAdminService().xmlLoadFasterXML(session);
    }

    @Override
    public void adminXmlSaveFasterXML(@Nullable final String sessionBASE64) throws Exception {
        checkSession(sessionBASE64, true);
        serviceLocator.getAdminService().xmlSaveFasterXML();
    }

    @Override
    public void adminJsonSaveJaxB(@Nullable final String sessionBASE64) throws Exception {
        checkSession(sessionBASE64, true);
        serviceLocator.getAdminService().jsonSaveJaxB();
    }

    @Override
    public void adminJsonLoadJaxB(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, true);
        serviceLocator.getAdminService().jsonLoadJaxB(session);
    }

    @Override
    public void adminXmlSaveJaxB(@Nullable final String sessionBASE64) throws Exception {
        checkSession(sessionBASE64, true);
        serviceLocator.getAdminService().xmlSaveJaxB();
    }

    @Override
    public void adminXmlLoadJaxB(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, true);
        serviceLocator.getAdminService().xmlLoadJaxB(session);
    }

    public void projectPersistList(
            @Nullable final String sessionBASE64,
            @Nullable final List<ProjectDTO> projects
    ) throws Exception {
        checkSession(sessionBASE64, true);
        serviceLocator.getProjectService().persist(ProjectDTO.getList(projects, serviceLocator));
    }

    @Override
    public void projectRemoveAll(@Nullable final String sessionBASE64) throws Exception {
        checkSession(sessionBASE64, true);
        serviceLocator.getProjectService().removeAll();
    }

    @Nullable
    @Override
    public List<ProjectDTO> projectFindAll(@Nullable final String sessionBASE64) throws Exception {
        checkSession(sessionBASE64, true);
        return Project.getListDTO(serviceLocator.getProjectService().findAll());
    }

    @Override
    public void taskPersistList(
            @Nullable final String sessionBASE64,
            @Nullable final List<TaskDTO> tasks
    ) throws Exception {
        checkSession(sessionBASE64, true);
        serviceLocator.getTaskService().persist(TaskDTO.getList(tasks, serviceLocator));
    }

    @Nullable
    @Override
    public List<TaskDTO> taskFindAll(@Nullable final String sessionBASE64) throws Exception {
        checkSession(sessionBASE64, true);
        return Task.getListDTO(serviceLocator.getTaskService().findAll());
    }

    @Override
    public void taskRemoveAll(@Nullable final String sessionBASE64) throws Exception {
        checkSession(sessionBASE64, true);
        serviceLocator.getTaskService().removeAll();
    }

    @Override
    public void userPersistList(
            @Nullable final String sessionBASE64,
            @Nullable final List<UserDTO> users
    ) throws Exception {
        checkSession(sessionBASE64, true);
        serviceLocator.getUserService().persist(UserDTO.getList(users, serviceLocator));
    }

    @Nullable
    @Override
    public List<UserDTO> userFindAll(@Nullable final String sessionBASE64) throws Exception {
        checkSession(sessionBASE64, true);
        return User.getListDTO(serviceLocator.getUserService().findAll());
    }

    @Override
    public void userRemoveAll(@Nullable final String sessionBASE64) throws Exception {
        checkSession(sessionBASE64, true);
        serviceLocator.getUserService().removeAll();
    }
}
