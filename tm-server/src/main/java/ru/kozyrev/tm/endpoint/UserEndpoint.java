package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.IUserEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.constant.ServerConst;
import ru.kozyrev.tm.dto.UserDTO;
import ru.kozyrev.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.kozyrev.tm.api.endpoint.IUserEndpoint")
@NoArgsConstructor
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {
    @NotNull
    public final static String URL = ServerConst.USER_URL;

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO userFindOne(
            @Nullable final String sessionBASE64,
            @Nullable final String userId
    ) throws Exception {
        checkSession(sessionBASE64, false);
        return User.getDTO(serviceLocator.getUserService().findOne(userId));
    }

    @Override
    @WebMethod
    public void userPersist(@Nullable final UserDTO userDTO) throws Exception {
        serviceLocator.getUserService().persistSaltPass(UserDTO.getUser(userDTO, serviceLocator));
    }

    @Override
    @WebMethod
    public void userMerge(
            @Nullable final String sessionBASE64,
            @Nullable final UserDTO userDTO
    ) throws Exception {
        checkSession(sessionBASE64, false);
        serviceLocator.getUserService().mergeSaltPass(UserDTO.getUser(userDTO, serviceLocator));
    }

    @WebMethod
    @Override
    public void userRemove(
            @Nullable final String sessionBASE64,
            @Nullable final String userId
    ) throws Exception {
        checkSession(sessionBASE64, false);
        serviceLocator.getUserService().remove(userId);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO userFindOneByLogin(
            @Nullable final String sessionBASE64,
            @Nullable final String login
    ) throws Exception {
        checkSession(sessionBASE64, false);
        return User.getDTO(serviceLocator.getUserService().findOneByLogin(login));
    }

    @Override
    @WebMethod
    public void userUpdatePassword(
            @Nullable final String sessionBASE64,
            @Nullable final UserDTO userDTO,
            @Nullable final String checkPassword
    ) throws Exception {
        checkSession(sessionBASE64, false);
        @Nullable final User user = UserDTO.getUser(userDTO, serviceLocator);
        serviceLocator.getUserService().updateUserPassword(user, checkPassword);
    }
}
