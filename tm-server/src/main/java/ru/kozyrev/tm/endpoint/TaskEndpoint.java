package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.ITaskEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.constant.ServerConst;
import ru.kozyrev.tm.dto.TaskDTO;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kozyrev.tm.api.endpoint.ITaskEndpoint")
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {
    @NotNull
    public final static String URL = ServerConst.TASK_URL;

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    public List<TaskDTO> taskFindAll(
            @Nullable final String sessionBASE64,
            @NotNull final Column column,
            @NotNull final Direction direction
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, false);
        return Task.getListDTO(serviceLocator.getTaskService().findAll(column, direction, session.getUser().getId()));
    }

    @Nullable
    @Override
    public List<TaskDTO> taskFindWord(
            @Nullable final String sessionBASE64,
            @NotNull final String word
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, false);
        return Task.getListDTO(serviceLocator.getTaskService().findWord(word, session.getUser().getId()));
    }

    public void taskPersist(
            @Nullable final String sessionBASE64,
            @Nullable final TaskDTO taskDTO
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, false);
        @Nullable final Task task = TaskDTO.getTask(taskDTO, serviceLocator);
        serviceLocator.getTaskService().persist(task, session.getUser().getId());
    }

    public void taskMerge(
            @Nullable final String sessionBASE64,
            @Nullable final TaskDTO taskDTO
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, false);
        @Nullable final Task task = TaskDTO.getTask(taskDTO, serviceLocator);
        serviceLocator.getTaskService().merge(task, session.getUser().getId());
    }

    public void taskRemove(
            @Nullable final String sessionBASE64,
            @Nullable final String taskId
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, false);
        serviceLocator.getTaskService().remove(taskId, session.getUser().getId());
    }

    @Override
    public void taskRemoveAll(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, false);
        serviceLocator.getTaskService().removeAll(session.getUser().getId());
    }

    @Override
    public void taskRemoveAllByProject(
            @Nullable final String sessionBASE64,
            @Nullable final String projectId
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, false);
        serviceLocator.getTaskService().removeAll(projectId, session.getUser().getId());
    }
}
