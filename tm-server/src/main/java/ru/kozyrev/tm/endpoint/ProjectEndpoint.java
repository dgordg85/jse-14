package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.IProjectEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.constant.ServerConst;
import ru.kozyrev.tm.dto.ProjectDTO;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kozyrev.tm.api.endpoint.IProjectEndpoint")
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {
    @NotNull
    public final static String URL = ServerConst.PROJECT_URL;

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public List<ProjectDTO> projectFindAll(
            @Nullable final String sessionBASE64,
            @NotNull final Column column,
            @NotNull final Direction direction
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, false);
        return Project.getListDTO(serviceLocator.getProjectService().findAll(column, direction, session.getUser().getId()));
    }

    @Nullable
    @Override
    public List<ProjectDTO> projectFindWord(
            @Nullable final String sessionBASE64,
            @NotNull final String word
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, false);
        return Project.getListDTO(serviceLocator.getProjectService().findWord(word, session.getUser().getId()));
    }

    @Override
    public void projectPersist(
            @Nullable final String sessionBASE64,
            @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, false);
        @Nullable final Project project = ProjectDTO.getProject(projectDTO, serviceLocator);
        serviceLocator.getProjectService().persist(project, session.getUser().getId());
    }

    @Override
    public void projectMerge(
            @Nullable final String sessionBASE64,
            @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, false);
        @Nullable final Project project = ProjectDTO.getProject(projectDTO, serviceLocator);
        serviceLocator.getProjectService().merge(project, session.getUser().getId());
    }

    @Override
    public void projectRemove(
            @Nullable final String sessionBASE64,
            @Nullable final String projectId
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, false);
        serviceLocator.getProjectService().remove(projectId, session.getUser().getId());
    }

    @Override
    public void projectRemoveAll(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64, false);
        serviceLocator.getProjectService().removeAll(session.getUser().getId());
    }
}
