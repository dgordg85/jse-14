package ru.kozyrev.tm.util;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.entity.User;

import javax.persistence.EntityManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public final class ConnectUtil {
    public static final EntityManagerFactory getEntityManagerFactory() throws IOException {
        @NotNull final Properties properties = new Properties();
        @NotNull final InputStream fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "connection.properties");
        properties.load(fis);

        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, properties.getProperty("driver"));
        settings.put(Environment.URL, properties.getProperty("url"));
        settings.put(Environment.USER, properties.getProperty("username"));
        settings.put(Environment.PASS, properties.getProperty("password"));
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.C3P0_MAX_SIZE, "100");
        settings.put(Environment.C3P0_MIN_SIZE, "5");
        settings.put(Environment.FORMAT_SQL, "true");
        //settings.put(Environment.JTA_PLATFORM, "org.hibernate.service.jta.platform.internal.WeblogicJtaPlatform");

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}
