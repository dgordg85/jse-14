package ru.kozyrev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.exception.entity.EntityException;

import java.util.Date;

public final class DateUtil {
    public static final java.sql.Date getSQLDate(@Nullable final Date date) throws Exception {
        if (date == null) {
            throw new EntityException();
        }
        @NotNull final java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        return sqlDate;
    }
}
