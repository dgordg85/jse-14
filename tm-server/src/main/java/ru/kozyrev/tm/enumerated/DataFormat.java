package ru.kozyrev.tm.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Getter
@RequiredArgsConstructor
public enum DataFormat {
    BIN(".bin"),
    XML(".xml"),
    JSON(".json");

    @NotNull
    private final String expansion;
}
