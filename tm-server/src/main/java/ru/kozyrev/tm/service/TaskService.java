package ru.kozyrev.tm.service;


import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.ITaskRepository;
import ru.kozyrev.tm.api.service.ITaskService;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.DocumentStatus;
import ru.kozyrev.tm.exception.entity.*;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
public final class TaskService extends AbstractObjectService<Task> implements ITaskService {
    @Override
    public final void persist(@Nullable final Task task) throws Exception {
        if (task == null) {
            throw new EmptyEntityException();
        }
        if (task.getId() == null || task.getId().isEmpty()) {
            task.setId(UUID.randomUUID().toString());
        }
        if (task.getName() == null || task.getName().isEmpty()) {
            throw new NameException();
        }
        if (task.getDescription() == null || task.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (task.getProject() == null) {
            throw new IndexException();
        }
        if (task.getDateStart() == null) {
            throw new DateException();
        }
        if (task.getDateFinish() == null) {
            throw new DateException();
        }
        if (task.getStatus() == null) {
            task.setStatus(DocumentStatus.PLANNED);
        }
        task.setTimestamp(System.currentTimeMillis());
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        getRepository(em).persist(task);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public final void merge(@Nullable final Task task, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (task == null) {
            throw new EmptyEntityException();
        }
        if (task.getId() == null || task.getId().isEmpty()) {
            throw new EntityException();
        }
        @Nullable final Task taskUpdate = this.findOne(task.getId(), userId);
        if (taskUpdate == null) {
            persist(task, userId);
            return;
        }
        if (task.getName() != null && !task.getName().isEmpty()) {
            taskUpdate.setName(task.getName());
        }
        if (task.getProject() != null) {
            taskUpdate.setProject(task.getProject());
        }
        if (task.getDescription() != null && !task.getDescription().isEmpty()) {
            taskUpdate.setDescription(task.getDescription());
        }
        if (task.getDateStart() != null) {
            taskUpdate.setDateStart(task.getDateStart());
        }
        if (task.getDateFinish() != null) {
            taskUpdate.setDateFinish(task.getDateFinish());
        }
        if (task.getStatus() != null) {
            taskUpdate.setStatus(task.getStatus());
        }

        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        getRepository(em).merge(taskUpdate);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public final List<Task> findAll(@Nullable final String projectId, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        @Nullable final List<Task> list = getRepository(em).findAllByProjectId(projectId, userId);
        em.getTransaction().commit();
        em.close();
        return list;
    }

    @Override
    public final void removeAll(@Nullable final String projectId, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        @NotNull final List<Task> list = findAll(projectId, userId);
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        getRepository(em).removeAll(list);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    public final ITaskRepository getRepository(@NotNull final EntityManager em) {
        return new TaskRepository(em);
    }
}
