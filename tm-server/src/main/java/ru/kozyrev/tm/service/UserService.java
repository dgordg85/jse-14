package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IUserRepository;
import ru.kozyrev.tm.api.service.IUserService;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.entity.EmptyEntityException;
import ru.kozyrev.tm.exception.entity.EntityException;
import ru.kozyrev.tm.exception.user.*;
import ru.kozyrev.tm.repository.UserRepository;
import ru.kozyrev.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.UUID;

@NoArgsConstructor
public final class UserService extends AbstractEntityService<User> implements IUserService {
    @Override
    public final void persist(@Nullable final User user) throws Exception {
        if (user == null) {
            throw new EmptyEntityException();
        }
        if (user.getId() == null || user.getId().isEmpty()) {
            user.setId(UUID.randomUUID().toString());
        }
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            throw new UserLoginEmptyException();
        }
        @Nullable final User userInDB = findOneByLogin(user.getLogin());
        if (userInDB != null) {
            throw new UserLoginTakenException();
        }
        if (isPassEmpty(user.getPasswordHash())) {
            throw new UserPasswordEmptyException();
        }
        if (user.getRoleType() == null) {
            user.setRoleType(RoleType.USER);
        }

        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        getRepository(em).persist(user);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public final void merge(@Nullable final User user) throws Exception {
        if (user == null) {
            throw new EmptyEntityException();
        }
        if (user.getId() == null || user.getId().isEmpty()) {
            throw new UserEmptyException();
        }
        @Nullable final User userUpdate = this.findOne(user.getId());
        if (userUpdate == null) {
            this.persist(user);
            return;
        }
        if (user.getLogin() != null && !user.getLogin().isEmpty()) {
            userUpdate.setLogin(user.getLogin());
        }
        if (user.getPasswordHash() != null && !isPassEmpty(user.getPasswordHash())) {
            userUpdate.setPasswordHash(user.getPasswordHash());
        }
        if (user.getRoleType() != null) {
            userUpdate.setRoleType(user.getRoleType());
        }
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        getRepository(em).merge(userUpdate);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public final void persistSaltPass(@Nullable final User user) throws Exception {
        @NotNull final User userSaltPass = saltPass(user);
        this.persist(userSaltPass);
    }

    @Override
    public final void mergeSaltPass(@Nullable final User user) throws Exception {
        if (user == null) {
            throw new EntityException();
        }
        if (user.getId() == null || user.getId().isEmpty()) {
            throw new EntityException();
        }

        if (!isPassEmpty(user.getPasswordHash())) {
            @Nullable final User userDb = findOne(user.getId());
            if (userDb == null) {
                this.persistSaltPass(user);
                return;
            }
            @Nullable final String newPassword = checkPasswordTrueGetCycle(userDb, user.getPasswordHash());
            if (newPassword == null) {
                user.setPasswordHash(HashUtil.getCycleHash(user.getPasswordHash()));
            }
        }
        this.merge(user);
    }

    @Nullable
    @Override
    public final User findOneByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) {
            return null;
        }
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        @Nullable final User result = getRepository(em).findOneByLogin(login);
        em.getTransaction().commit();
        em.close();
        return result;
    }

    @Override
    public final void updateUserPassword(
            @Nullable final User user,
            @Nullable final String checkPassword
    ) throws Exception {
        if (user == null) {
            throw new UserEmptyException();
        }
        @Nullable final String cyclePass = checkPasswordTrueGetCycle(user, checkPassword);
        if (cyclePass == null) {
            throw new UserPasswordWrongException();
        }
        @NotNull final User userSaltPass = this.saltPass(user);
        this.merge(userSaltPass);
    }

    @Nullable
    private final String checkPasswordTrueGetCycle(
            @NotNull final User user,
            @Nullable final String hashPassword
    ) throws Exception {
        if (isPassEmpty(hashPassword)) {
            return null;
        }
        @NotNull final String cycleHashPassword = HashUtil.getCycleHash(hashPassword);
        if (cycleHashPassword.equals(user.getPasswordHash())) {
            return cycleHashPassword;
        }
        return null;
    }

    @NotNull
    private final User saltPass(@Nullable final User user) throws Exception {
        if (user == null) {
            throw new EmptyEntityException();
        }
        @Nullable final String userPass = user.getPasswordHash();
        if (isPassEmpty(userPass)) {
            throw new UserPasswordEmptyException();
        }
        user.setPasswordHash(HashUtil.getCycleHash(userPass));
        return user;
    }

    @NotNull
    public final IUserRepository getRepository(@NotNull final EntityManager em) {
        return new UserRepository(em);
    }
}
