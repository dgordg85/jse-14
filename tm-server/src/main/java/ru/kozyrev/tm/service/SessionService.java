package ru.kozyrev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.ISessionRepository;
import ru.kozyrev.tm.api.service.ISessionService;
import ru.kozyrev.tm.constant.ServerConst;
import ru.kozyrev.tm.dto.SessionDTO;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.entity.EmptyEntityException;
import ru.kozyrev.tm.exception.session.SessionCreateException;
import ru.kozyrev.tm.exception.session.SessionNotValidException;
import ru.kozyrev.tm.exception.user.UserLoginEmptyException;
import ru.kozyrev.tm.exception.user.UserLoginNotRegistryException;
import ru.kozyrev.tm.exception.user.UserPasswordEmptyException;
import ru.kozyrev.tm.exception.user.UserPasswordWrongException;
import ru.kozyrev.tm.repository.SessionRepository;
import ru.kozyrev.tm.util.AesUtil;
import ru.kozyrev.tm.util.HashUtil;
import ru.kozyrev.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.util.UUID;

public final class SessionService extends AbstractEntityService<Session> implements ISessionService {
    @Override
    public final void persist(@Nullable final Session session) throws Exception {
        if (session == null) {
            throw new SessionCreateException();
        }
        if (session.getId() == null || session.getId().isEmpty()) {
            session.setId(UUID.randomUUID().toString());
        }
        if (session.getRoleType() == null) {
            throw new EmptyEntityException();
        }
        if (session.getTimestamp() == null) {
            throw new EmptyEntityException();
        }
        if (session.getSignature() == null || session.getSignature().isEmpty()) {
            throw new EmptyEntityException();
        }
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        getRepository(em).persist(session);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public final void merge(@Nullable final Session session) throws Exception {
        if (session == null) {
            throw new SessionNotValidException();
        }
        if (session.getId() == null) {
            throw new EmptyEntityException();
        }
        @Nullable final Session sessionDB = findOne(session.getId());
        if (sessionDB == null) {
            this.persist(session);
            return;
        }
        if (session.getTimestamp() != null) {
            sessionDB.setTimestamp(session.getTimestamp());
        }
        if (session.getRoleType() != null) {
            sessionDB.setRoleType(session.getRoleType());
        }
        @NotNull final Session sessionSign = SignatureUtil.signSession(sessionDB);
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        getRepository(em).merge(sessionSign);
        em.getTransaction().commit();
        em.close();
    }

    @Nullable
    public final Session findOneByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            return null;
        }
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        @Nullable final Session session = getRepository(em).findOneByUserId(userId);
        em.getTransaction().commit();
        em.close();
        return session;
    }

    @NotNull
    @Override
    public final String openSession(
            @Nullable final String login,
            @Nullable final String hashPassword
    ) throws Exception {
        if (login == null || login.isEmpty()) {
            throw new UserLoginEmptyException();
        }
        if (isPassEmpty(hashPassword)) {
            throw new UserPasswordEmptyException();
        }
        @Nullable final User user = serviceLocator.getUserService().findOneByLogin(login);
        if (user == null) {
            throw new UserLoginNotRegistryException();
        }
        @NotNull final String saltHashPassword = HashUtil.getCycleHash(hashPassword);
        if (!saltHashPassword.equals(user.getPasswordHash())) {
            throw new UserPasswordWrongException();
        }
        @Nullable Session session = findOneByUserId(user.getId());
        if (isSessionDead(session)) {
            session = null;
        }
        if (session == null) {
            @NotNull final Session newSession = createSession(user);
            this.persist(newSession);
            session = this.findOne(newSession.getId());
            if (session == null) {
                throw new SessionCreateException();
            }
        }
        session.setUser(user);
        @Nullable final SessionDTO sessionDTO = Session.getDTO(session);
        @Nullable final String sessionBASE64 = AesUtil.encryptSession(sessionDTO);
        if (sessionBASE64 == null) {
            throw new SessionCreateException();
        }
        return sessionBASE64;
    }

    @Override
    public final void closeSession(@Nullable final Session session) throws Exception {
        if (session == null || session.getId() == null) {
            throw new SessionNotValidException();
        }
        this.remove(session.getId());
    }

    @Override
    public final boolean isValid(@Nullable final Session session, final boolean checkForbidden) throws Exception {
        if (session == null) {
            return false;
        }
        @Nullable final Session serverSession = this.findOne(session.getId());
        if (serverSession == null || serverSession.getRoleType() == null) {
            return false;
        }
        if (session.getSignature() == null || session.getSignature().isEmpty()) {
            return false;
        }
        if (!session.getSignature().equals(serverSession.getSignature())) {
            return false;
        }
        if (isSessionDead(session)) {
            return false;
        }
        return !checkForbidden || !serverSession.getRoleType().equals(RoleType.ADMIN);
    }

    private final boolean isSessionDead(@Nullable final Session session) throws Exception {
        if (session == null) {
            return true;
        }
        @Nullable final Session serverSession = this.findOne(session.getId());
        if (serverSession == null || serverSession.getTimestamp() == null) {
            return true;
        }
        final long sessionTime = System.currentTimeMillis() - serverSession.getTimestamp();
        if (ServerConst.SESSION_LIFETIME < sessionTime) {
            this.remove(session.getId());
            return true;
        }
        return false;
    }

    @NotNull
    private final Session createSession(@NotNull final User user) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(UUID.randomUUID().toString());
        session.setUser(user);
        session.setRoleType(user.getRoleType());
        session.setTimestamp(System.currentTimeMillis());
        @NotNull final Session signSession = SignatureUtil.signSession(session);
        return signSession;
    }

    @NotNull
    public final ISessionRepository getRepository(@NotNull final EntityManager em) {
        return new SessionRepository(em);
    }
}
