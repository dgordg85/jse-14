package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IProjectRepository;
import ru.kozyrev.tm.api.service.IProjectService;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.DocumentStatus;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.entity.*;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.UUID;

@NoArgsConstructor
public final class ProjectService extends AbstractObjectService<Project> implements IProjectService {
    @Override
    public final void persist(@Nullable final Project project) throws Exception {
        if (project == null) {
            throw new EmptyEntityException();
        }
        if (project.getId() == null || project.getId().isEmpty()) {
            project.setId(UUID.randomUUID().toString());
        }
        if (project.getName() == null || project.getName().isEmpty()) {
            throw new NameException();
        }
        if (project.getDescription() == null || project.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }
        if (project.getStatus() == null) {
            project.setStatus(DocumentStatus.PLANNED);
        }
        project.setTimestamp(System.currentTimeMillis());
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        getRepository(em).persist(project);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public final void merge(
            @Nullable final Project project,
            @Nullable final String userId
    ) throws Exception {
        if (project == null) {
            throw new ServiceFailException();
        }
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (project.getId() == null) {
            throw new EntityException();
        }
        @Nullable final Project projectUpdate = this.findOne(project.getId(), userId);
        if (projectUpdate == null) {
            persist(project, userId);
            return;
        }
        if (project.getName() != null && !project.getName().isEmpty()) {
            projectUpdate.setName(project.getName());
        }
        if (project.getDescription() != null && !project.getDescription().isEmpty()) {
            projectUpdate.setDescription(project.getDescription());
        }
        if (project.getDateStart() != null) {
            projectUpdate.setDateStart(project.getDateStart());
        }
        if (project.getDateFinish() != null) {
            projectUpdate.setDateFinish(project.getDateFinish());
        }
        if (project.getStatus() != null) {
            projectUpdate.setStatus(project.getStatus());
        }

        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        getRepository(em).merge(projectUpdate);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    public final IProjectRepository getRepository(@NotNull final EntityManager em) {
        return new ProjectRepository(em);
    }
}
