package ru.kozyrev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IRepository;
import ru.kozyrev.tm.api.service.IService;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.AbstractEntity;
import ru.kozyrev.tm.exception.data.NoDataException;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {
    @NotNull
    protected ServiceLocator serviceLocator;

    @Override
    public abstract void persist(@Nullable final T entity) throws Exception;

    @Override
    public void persist(@Nullable final List<T> entities) throws Exception {
        if (entities == null) {
            throw new NoDataException();
        }
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        for (final T entity : entities) {
            if (entity == null) {
                continue;
            }
            getRepository(em).persist(entity);
        }
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public final List<T> findAll() throws Exception {
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        @Nullable final List<T> list = getRepository(em).findAll();
        em.getTransaction().commit();
        em.close();
        return list;
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final List<T> list = findAll();

        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        getRepository(em).removeAll(list);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public final void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract IRepository<T> getRepository(@NotNull final EntityManager em);

    @NotNull
    public final EntityManager getEntityManager() {
        return serviceLocator.getEntityManagerFactory().createEntityManager();
    }
}
