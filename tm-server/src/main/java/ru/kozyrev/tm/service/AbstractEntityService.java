package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.IEntityService;
import ru.kozyrev.tm.entity.AbstractEntity;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.util.HashUtil;

import javax.persistence.EntityManager;

@NoArgsConstructor
public abstract class AbstractEntityService<T extends AbstractEntity> extends AbstractService<T> implements IEntityService<T> {

    @Override
    public abstract void persist(@Nullable final T entity) throws Exception;

    @Override
    public abstract void merge(@Nullable final T entity) throws Exception;

    @Nullable
    @Override
    public final T findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new IndexException();
        }
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        @Nullable final T entity = getRepository(em).findOne(id);
        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public final void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new IndexException();
        }
        @Nullable final T entity = findOne(id);
        if (entity == null) {
            throw new IndexException();
        }
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        getRepository(em).remove(entity);
        em.getTransaction().commit();
        em.close();
    }

    protected final boolean isPassEmpty(@Nullable final String hashPassword) {
        return hashPassword == null || hashPassword.isEmpty() || hashPassword.equals(HashUtil.EMPTY_PASSWORD);
    }
}
