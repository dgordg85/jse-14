package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.IObjectService;
import ru.kozyrev.tm.entity.AbstractObject;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.exception.entity.EmptyEntityException;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractObjectService<T extends AbstractObject> extends AbstractService<T> implements IObjectService<T> {
    @Override
    public abstract void persist(@Nullable final T object) throws Exception;

    @Override
    public final void persist(@Nullable final T object, @Nullable final String userId) throws Exception {
        if (object == null) {
            throw new EmptyEntityException();
        }
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        @Nullable final User user = new UserRepository(em).findOne(userId);
        em.getTransaction().commit();
        em.close();
        if (user == null) {
            throw new EmptyEntityException();
        }
        object.setUser(user);
        this.persist(object);
    }

    @Override
    public abstract void merge(@Nullable final T object, @Nullable final String userId) throws Exception;

    @Nullable
    @Override
    public final T findOne(
            @Nullable final String id,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (id == null || id.isEmpty()) {
            return null;
        }
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        @Nullable final T entity = getRepository(em).findOne(id, userId);
        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @NotNull
    @Override
    public final List<T> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        @Nullable final List<T> list = getRepository(em).findAllByUserId(userId);
        em.getTransaction().commit();
        em.close();
        return list;
    }

    @NotNull
    @Override
    public final List<T> findAll(
            @NotNull final Column column,
            @NotNull final Direction direction,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        @Nullable final List<T> list;
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        switch (column) {
            case DATE_START:
                if (direction == Direction.DESC) {
                    list = getRepository(em).findAllByDateStartDesc(userId);
                } else {
                    list = getRepository(em).findAllByDateStart(userId);
                }
                break;
            case DATE_FINISH:
                if (direction == Direction.DESC) {
                    list = getRepository(em).findAllByDateFinishDesc(userId);
                } else {
                    list = getRepository(em).findAllByDateFinish(userId);
                }
                break;
            case STATUS:
                if (direction == Direction.DESC) {
                    list = getRepository(em).findAllByStatusDesc(userId);
                } else {
                    list = getRepository(em).findAllByStatus(userId);
                }
                break;
            default:
                if (direction == Direction.DESC) {
                    list = getRepository(em).findAllByNumDesc(userId);
                } else {
                    list = getRepository(em).findAllByNum(userId);
                }
        }
        em.getTransaction().commit();
        em.close();
        return list;
    }

    @Override
    public final void remove(@Nullable final String objectId, @Nullable final String userId) throws Exception {
        if (objectId == null || objectId.isEmpty()) {
            throw new EmptyEntityException();
        }
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        @Nullable final T object = getRepository(em).findOne(objectId, userId);
        if (object == null) {
            throw new IndexException();
        }
        getRepository(em).remove(object);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final List<T> list = findAll();

        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        getRepository(em).removeAll(list);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public final void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        @NotNull final List<T> list = findAll(userId);
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        getRepository(em).removeAll(list);
        em.getTransaction().commit();
        em.close();
    }

    @Nullable
    @Override
    public List<T> findWord(
            @Nullable final String word,
            @Nullable final String userId
    ) throws Exception {
        if (word == null || word.isEmpty()) {
            return null;
        }
        @NotNull final String likeWord = "%" + word + "%";
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        @NotNull final EntityManager em = getEntityManager();
        em.getTransaction().begin();
        @Nullable final List<T> list = getRepository(em).findWord(likeWord, userId);
        em.getTransaction().commit();
        em.close();
        return list;
    }
}
