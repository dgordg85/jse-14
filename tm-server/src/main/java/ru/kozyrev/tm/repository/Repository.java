package ru.kozyrev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IRepository;
import ru.kozyrev.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class Repository<T extends AbstractEntity> implements IRepository<T> {
    @NotNull
    protected final EntityManager em;

    public Repository(@NotNull final EntityManager em) {
        this.em = em;
    }

    @Nullable
    @Override
    public final T findOne(@NotNull final String id) {
        return em.find(getClassName(), id);
    }

    @Override
    public final void persist(@NotNull final T entity) {
        em.persist(entity);
    }

    @Override
    public final void merge(@NotNull final T entity) {
        em.merge(entity);
    }

    @Override
    public void remove(@NotNull final T entity) {
        em.remove(entity);
    }

    @Override
    public void removeAll(@NotNull final List<T> list) {
        for (final T entity : list) {
            em.remove(entity);
        }
    }

    @NotNull
    @Override
    public abstract List<T> findAll() throws Exception;

    @NotNull
    public abstract Class<T> getClassName();
}
