package ru.kozyrev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.ITaskRepository;
import ru.kozyrev.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends Repository<Task> implements ITaskRepository {
    @NotNull
    private static final Class<Task> CLASS_NAME = Task.class;

    public TaskRepository(@NotNull final EntityManager em) {
        super(em);
    }

    @Nullable
    @Override
    public final Task findOne(@NotNull final String id, @NotNull final String userId) {
        return em.createQuery("SELECT O FROM Task O WHERE O.id=?1 AND O.user.id=?2", getClassName())
                .setParameter(1, id)
                .setParameter(2, userId)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public final List<Task> findAllByUserId(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery("SELECT O FROM Task WHERE O.user.id=?1 " +
                "ORDER BY O.timestamp", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Task> findAllByProjectId(@NotNull final String projectId, @NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT T FROM Task T WHERE T.project.id=?1 AND T.user.id=?2", getClassName())
                .setParameter(1, projectId)
                .setParameter(2, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Task> findWord(@NotNull final String word, @NotNull final String userId) {
        return new ArrayList<>(em.createQuery("SELECT O FROM Task O WHERE O.user.id=?1 AND " +
                "(O.name LIKE ?2 OR O.description LIKE ?2) ORDER BY O.timestamp", getClassName())
                .setParameter(1, userId)
                .setParameter(2, word)
                .getResultList());
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return new ArrayList<>(em.createQuery("SELECT T FROM Task T", getClassName())
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Task> findAllByNum(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT T FROM Task T WHERE T.user.id=?1 " +
                        "ORDER BY T.timestamp", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Task> findAllByDateStart(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT T FROM Task T WHERE T.project.id=?1 AND T.user.id=?2 " +
                        "ORDER BY T.dateStart, T.timestamp", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Task> findAllByDateFinish(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT T FROM Task T WHERE T.user.id=?1 " +
                        "ORDER BY T.dateFinish, T.timestamp", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Task> findAllByStatus(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT T FROM Task T WHERE T.user.id=?1 " +
                        "ORDER BY T.status, T.timestamp", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Task> findAllByNumDesc(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT T FROM Task T WHERE T.user.id=?1 " +
                        "ORDER BY T.timestamp DESC", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Task> findAllByDateStartDesc(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT T FROM Task T WHERE T.user.id=?1 " +
                        "ORDER BY T.dateStart DESC, T.timestamp DESC", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Task> findAllByDateFinishDesc(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT T FROM Task T WHERE T.user.id=?1 " +
                        "ORDER BY T.dateFinish DESC, T.timestamp DESC", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Task> findAllByStatusDesc(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT T FROM Task T WHERE T.user.id=?1 " +
                        "ORDER BY T.status DESC, T.timestamp DESC", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public Class<Task> getClassName() {
        return CLASS_NAME;
    }
}
