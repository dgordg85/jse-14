package ru.kozyrev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IProjectRepository;
import ru.kozyrev.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends Repository<Project> implements IProjectRepository {
    @NotNull
    private static final Class<Project> CLASS_NAME = Project.class;

    public ProjectRepository(@NotNull final EntityManager em) {
        super(em);
    }

    @Nullable
    @Override
    public final Project findOne(@NotNull final String id, @NotNull final String userId) {
        return em.createQuery("SELECT O FROM Project O WHERE O.id=?1 AND O.user.id=?2", getClassName())
                .setParameter(1, id)
                .setParameter(2, userId)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public final List<Project> findAllByUserId(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery("SELECT O FROM Project WHERE O.user.id=?1 " +
                "ORDER BY O.timestamp", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Project> findWord(@NotNull final String word, @NotNull final String userId) {
        return new ArrayList<>(em.createQuery("SELECT O FROM Project O WHERE O.user.id=?1 AND " +
                "(O.name LIKE ?2 OR O.description LIKE ?2) ORDER BY O.timestamp", getClassName())
                .setParameter(1, userId)
                .setParameter(2, word)
                .getResultList());
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return new ArrayList<>(em.createQuery("SELECT P FROM Project P", getClassName())
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Project> findAllByNum(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT P FROM Project P WHERE P.user.id=?1 " +
                        "ORDER BY P.timestamp", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Project> findAllByDateStart(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT P FROM Project P WHERE P.user.id=?1 " +
                        "ORDER BY P.dateStart, P.timestamp", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Project> findAllByDateFinish(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT P FROM Project P WHERE P.user.id=?1 " +
                        "ORDER BY P.dateFinish, P.timestamp", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Project> findAllByStatus(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT P FROM Project P WHERE P.user.id=?1 " +
                        "ORDER BY P.status, P.timestamp", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Project> findAllByNumDesc(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT P FROM Project P WHERE P.user.id=?1 " +
                        "ORDER BY P.timestamp DESC", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Project> findAllByDateStartDesc(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT P FROM Project P WHERE P.user.id=?1 " +
                        "ORDER BY P.dateStart DESC, P.timestamp DESC", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Project> findAllByDateFinishDesc(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT P FROM Project P WHERE P.user.id=?1 " +
                        "ORDER BY P.dateFinish DESC, P.timestamp DESC", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @NotNull
    @Override
    public final List<Project> findAllByStatusDesc(@NotNull final String userId) {
        return new ArrayList<>(em.createQuery(
                "SELECT P FROM Project P WHERE P.user.id=?1 " +
                        "ORDER BY P.status DESC, P.timestamp DESC", getClassName())
                .setParameter(1, userId)
                .getResultList());
    }

    @Override
    public @NotNull Class<Project> getClassName() {
        return CLASS_NAME;
    }
}
