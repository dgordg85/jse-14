package ru.kozyrev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IUserRepository;
import ru.kozyrev.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends Repository<User> implements IUserRepository {
    @NotNull
    private static final Class<User> CLASS_NAME = User.class;

    public UserRepository(@NotNull final EntityManager em) {
        super(em);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return new ArrayList<>(em.createQuery("SELECT U FROM User U", getClassName())
                .getResultList());
    }

    @Nullable
    @Override
    public final User findOneByLogin(@NotNull final String login) {
        return em.createQuery("SELECT U FROM User U WHERE U.login=?1", getClassName())
                .setParameter(1, login)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public Class<User> getClassName() {
        return CLASS_NAME;
    }
}
