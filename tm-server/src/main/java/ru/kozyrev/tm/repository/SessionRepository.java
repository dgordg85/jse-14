package ru.kozyrev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.ISessionRepository;
import ru.kozyrev.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends Repository<Session> implements ISessionRepository {
    @NotNull
    private static final Class<Session> CLASS_NAME = Session.class;

    public SessionRepository(@NotNull final EntityManager em) {
        super(em);
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return new ArrayList<>(em.createQuery("SELECT S FROM Session S", getClassName())
                .getResultList());
    }

    @Nullable
    @Override
    public final Session findOneByUserId(@NotNull final String userId) {
        return em.createQuery("SELECT S FROM Session S WHERE S.user.id=?1", getClassName())
                .setParameter(1, userId)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public Class<Session> getClassName() {
        return CLASS_NAME;
    }
}
